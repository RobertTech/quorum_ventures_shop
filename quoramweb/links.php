<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/vendor/font-awesome/css/fontawesome-all.min.css">
		<link rel="stylesheet" href="../assets/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="../assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="../assets/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="../assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../assets/css/theme.css">
		<link rel="stylesheet" href="../assets/css/theme-elements.css">
		<link rel="stylesheet" href="../assets/css/theme-blog.css">
		<link rel="stylesheet" href="../assets/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="../assets/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="../assets/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="../assets/vendor/rs-plugin/css/navigation.css">
		<link rel="stylesheet" href="../assets/vendor/circle-flip-slideshow/css/component.css">
		
		<!-- Demo CSS -->


		<!-- Skin CSS -->
		<link rel="stylesheet" href="../assets/css/skins/default.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../assets/css/custom.css">

		<!-- Head Libs -->
		<script src="../assets/vendor/modernizr/modernizr.min.js"></script>
		<script src="../assets/vendor/jquery/jquery.min.js"></script>
		<script src="../assets/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="../assets/vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<script src="../assets/vendor/popper/umd/popper.min.js"></script>
		<script src="../assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="../assets/vendor/common/common.min.js"></script>
		<script src="../assets/vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="../assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="../assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="../assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="../assets/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="../assets/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="../assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="../assets/vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../assets/js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="../assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="../assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="../assets/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js"></script>
		<script src="../assets/js/views/view.home.js"></script>
		
		<!-- Theme Custom -->
		<script src="../assets/js/custom.js"></script>
		<script src="../assets/js/jsurl.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../assets/js/theme.init.js"></script>
		<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>