<!-- Main navbar -->
<?php

$arry_uri=explode('/', $_SERVER['REQUEST_URI']);
$ordersactive=false;
$analyticsactive=false;

if($arry_uri['3']=="orders.php" || $arry_uri['3']=="orders-packed.php" || $arry_uri['3']=="orders-ontransit.php" || $arry_uri['3']=="orders-received.php")
{
$ordersactive=true;
$analyticsactive=false;
}
else if($arry_uri['3']=="dashboard.php" || $arry_uri['3']=="generalanalytics.php" || $arry_uri['3']=="dcanalytics.php" || $arry_uri['3']=="brandanalytics.php"||$arry_uri['3']=="toselling.php"||$arry_uri['3']=="monthlyanalytics.php")
{
$ordersactive=false;
$analyticsactive=true;
}
?>
<script type="text/javascript">
    if(localStorage.getItem('access_token')==0)
    {
     window.location = "../index.php";
    }
    
</script>
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="#"><img style="width:40px; height: 30px;" src="../assets/images/1.png"/></a>
        
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
            <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-git-compare"></i>
                        <span class="visible-xs-inline-block position-right">Git updates</span>
                        <span class="badge bg-warning-400">9</span>
                    </a>
            </li>

        </ul>

        <p class="navbar-text">
            <span class="label bg-success">Online</span>
            <img class="status-progresslog"  src="../assets/loader/loader.gif"/>
        </p>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <?php 
if($arry_uri['1']=="genman")
{
    ?>
    <a href="/genman/orders.php">
    <?php

}
else if($arry_uri['1']=="dir")
{
     
}
else
{
    ?>
     <a href="/cc/orders.php">
    <?php
}
                ?>
                    
                        <i class="icon-bubbles4"></i>
                       
                        <span class="badge bg-warning-400" style="background-color: #333" id="newordersplaceholder"></span>
                    </a>
                </li>
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img id="placehere2" src="../assets/images/placeholder.jpg" alt="">
                        <span id="nameUsers"></span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="userprofile.php"><i class="icon-user-plus"></i> My profile</a></li>
                         <li><a href="change.php"><i class="icon-user-plus"></i> Change password</a></li>
                        <li><a href="javascript: logout()"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>

        </div>
        <script>

            document.getElementById("nameUsers").innerHTML = localStorage.getItem('retailer_name');
            $(".status-progresslog").hide();
            
            function logout() {
                $(".status-progresslog").show();
                var url = base_url + "/user/logout";
                var formData = {
                    'phone': localStorage.getItem('phone'),
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                       
                        text: data['message'],
                        addclass: 'bg-success'
                    });
                    window.location = "../index.php";
                    localStorage.setItem('access_token',"")
                    $(".status-progresslog").hide();
                });
            }
        </script>
        <script type="text/javascript">
            if (localStorage.getItem('image_url')) {
                var elem = document.createElement("img");

                elem.setAttribute("src", localStorage.getItem('image_url'));
                elem.setAttribute("height", "768");
                elem.setAttribute("width", "1024");
                elem.setAttribute("alt", "Flower");
                document.getElementById("placehere2").appendChild(elem);
                var img= '<img class="profile-user-img img-responsive img-circle"  src="../assets/images/placeholder.jpg" alt="User profile picture">';
                document.getElementById("placehere3").appendChild(img);
            } 
            else {
                var elem = document.createElement("img");
                elem.setAttribute("src", "../assets/images/placeholder.jpg");
                elem.setAttribute("height", "10");
                elem.setAttribute("width", "10");
                elem.setAttribute("alt", "Flower");
                document.getElementById("placehere2").appendChild(elem);
            }



        </script>

    </div>
</div>
<!-- /main navbar -->
