<!DOCTYPE html>
<html lang="en">
<head>
  <title>Make Payment | Quorum Ventures</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

  <script src="../assets/js/jsurl.js"></script>

  <style type="text/css">

  	.greenboder
  	{
  		border: 1px;
  		border-color: #527C16;
  		border-style: solid;;
  	}
    .greenbackground
  	{
  		background-color:#527C16;
  		color:white; 
  	}

  	.navbar-default .navbar-nav > .active{
    color: #000;
    background: #d65c14;
}
.navbar-inverse .navbar-nav > .active > a,
.navbar-inverse .navbar-nav > .active > a:hover,
.navbar-inverse .navbar-nav > .active > a:focus {
  color: black;
  background-color: white;
  border-radius: 10px;


}
.loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
#loading-indicator {
  position: relative;
  width: 80px;
  height: 80px;
   margin: 0 auto;
  
}
.error
{
	color:red;
}
.modal-confirm {    
    color: #636363;
    width: 325px;
  }
  .modal-confirm .modal-content {
    padding: 20px;
    border-radius: 5px;
    border: none;
  }
  .modal-confirm .modal-header {
    border-bottom: none;   
        position: relative;
  }
  .modal-confirm h4 {
    text-align: center;
    font-size: 26px;
    margin: 30px 0 -15px;
  }
  .modal-confirm .form-control, .modal-confirm .btn {
    min-height: 40px;
    border-radius: 3px; 
  }
  .modal-confirm .close {
        position: absolute;
    top: -5px;
    right: -5px;
  } 
  .modal-confirm .modal-footer {
    border: none;
    text-align: center;
    border-radius: 5px;
    font-size: 13px;
  } 
  .modal-confirm .icon-box {
    color: #fff;    
    position: absolute;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: -70px;
    width: 95px;
    height: 95px;
    border-radius: 50%;
    z-index: 9;
    background: #82ce34;
    padding: 15px;
    text-align: center;
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
  }
  .modal-confirm .icon-box i {
    font-size: 58px;
    position: relative;
    top: 3px;
  }
  .modal-confirm.modal-dialog {
    margin-top: 80px;
  }
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
    background: #82ce34;
    text-decoration: none;
    transition: all 0.4s;
        line-height: normal;
        border: none;
    }
  .modal-confirm .btn:hover, .modal-confirm .btn:focus {
    background: #6fb32b;
    outline: none;
  }
  .trigger-btn {
    display: inline-block;
    margin: 100px auto;
  }
  </style>
   <script>
            $(document).ready(function () {
            	  $('#loading-indicator').hide();
                 $('#request_form').submit(function (e) {
                 	 
                    e.preventDefault();
                     $('#loading-indicator').show();
                     $clientname= $('#client_name').val();
                     $clientcompany= $('#client_company').val();
                     $clienttopic= $('#client_topic').val();
                     $clientdetails=$('#client_details').val();
                     $clientemail=$('#client_email').val();

                       var error="";
                       var errormsg="";
                       if($clientname=="")
                       {
                         
                         document.getElementById("nameerror").innerHTML ="Name is required";
                         error="true";
                       }
                       if($clientcompany=="")
                       {
                         
                         document.getElementById("companyerror").innerHTML ="Your company name is required";
                         error="true";
                       }
                       if($clienttopic=="")
                       {
                         
                          document.getElementById("topicerror").innerHTML ="Title Of your quote is Required";
                         error="true";
                       }
                         if($clientdetails=="")
                       {
                         
                         document.getElementById("detailserror").innerHTML ="Details Of your Quote are required";
                         error="true";	
                       }
                         if($clientemail=="")
                       {
            
                       	  document.getElementById("emailerror").innerHTML ="Email is required";
                       	 error="true";
                       }
                     
                 if(error=="true")
                 {
                 	$('#loading-indicator').hide();
                   console.log(error);
                 }
                 else
                 {    document.getElementById("nameerror").innerHTML ="";
                      document.getElementById("companyerror").innerHTML ="";
                      document.getElementById("topicerror").innerHTML ="";
                      document.getElementById("detailserror").innerHTML ="";
                      document.getElementById("emailerror").innerHTML ="";
                 	    $("#submit").addClass("disabled");
                        $(".status-progress-add").show();
                        console.log(new FormData(this));
                        $.ajax({
                        url: base_url2 + "Quoterequest/requestq",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                             $('#loading-indicator').hide();
                             $("#client_company").val("");
                             $("#client_name").val("");
                             $("#client_topic").val("");
                             $("#client_details").val("");
                             $("#client_email").val("");
                              $("#myModal").modal('show');
                             
                           

                        },
                        error: function () {}
                        });
                   
                 }
});
                 validate = function(vari)
                 {
                     if(vari=='cname'&& $('#client_company').val()=="")
                     {
                        document.getElementById("companyerror").innerHTML ="company name is required";
                     }
                     else if(vari=='cname'&& $('#client_company').val()!="")
                     {
                          document.getElementById("companyerror").innerHTML ="";
                     }

                     if(vari=='name'&& $('#client_name').val()=="")
                     {
                        document.getElementById("nameerror").innerHTML ="Your name is required";
                     }
                     else if(vari=='cname'&& $('#client_company').val()!="")
                     {
                       document.getElementById("nameerror").innerHTML ="";
                     }
                     if(vari=='email'&& $('#client_email').val()=="")
                     {
                        document.getElementById("emailerror").innerHTML ="Email is required";
                     }
                     else if(vari=='email'&& $('#client_company').val()!="")
                     {
                        document.getElementById("emailerror").innerHTML ="";
                     }
                     if(vari=='top'&& $('#client_topic').val()=="")
                     {
                        document.getElementById("topicerror").innerHTML ="Topic is required";
                     }
                     else if(vari=='top'&& $('#client_topic').val()!="")
                     {
                        document.getElementById("topicerror").innerHTML ="";
                     }
                     if(vari=='details'&& $('#client_details').val()=="")
                     {
                        document.getElementById("detailserror").innerHTML ="Details required";
                     }
                     else if(vari=='details'&& $('#client_details').val()!="")
                     {
                       document.getElementById("detailserror").innerHTML ="";
                     }
                 }
$('#example').DataTable();
});

            
            
        </script>
     

</head>
<body>

              <div class="header-wrapper clearfix"><!-- header wrapper -->
                                
                    <header id="header" class="header-8 search-sm">
    
    <div class="header-main">
        <div class="container">
            <div class="header-left">
                <h1 class="logo">    <a href="http://quorumventures.co.ke/" title="Quorum Ventures - Building a firm business network" rel="home">
                <img class="img-responsive standard-logo" src="//quorumventures.co.ke/wp-content/uploads/2018/08/Quroum-logo.jpeg" alt="Quorum Ventures" /><img class="img-responsive retina-logo" src="//quorumventures.co.ke/wp-content/uploads/2018/08/Quroum-logo.jpeg" alt="Quorum Ventures" style="display:none;" />            </a>
    </h1>            </div>
            <div class="header-center">
                <div id="main-menu">
                    <ul id="menu-main-menu" class="main-menu mega-menu show-arrow effect-down subeffect-fadein-left"><li id="nav-menu-item-541" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-143 current_page_item active narrow "><a href="http://quorumventures.co.ke/" class=" current">Home</a></li>
<li id="nav-menu-item-597" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/about-us/" class="">About Us</a></li>
<li id="nav-menu-item-879" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/our-portfolio/" class="">Our Portfolio</a></li>
<li id="nav-menu-item-684" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/670-2/" class="">Blog</a></li>
<li id="nav-menu-item-891" class="menu-item menu-item-type-custom menu-item-object-custom  narrow "><a href="http://shop.quorumventures.co.ke/quotes/requestquote.php" class="">Enquire</a></li>
<li id="nav-menu-item-892" class="menu-item menu-item-type-custom menu-item-object-custom  narrow "><a href="http://shop.quorumventures.co.ke/quotes/makepayment.php" class="">Pay</a></li>
<li id="nav-menu-item-791" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/contact/" class="">Contact Us</a></li>
</ul>                </div>
            </div>
            <div class="header-right search-popup">
                                <div class="">
                                        <a class="mobile-toggle"><i class="fa fa-reorder"></i></a>
                    <div class="block-nowrap">
                            <div class="searchform-popup">
        <a class="search-toggle"><i class="fa fa-search"></i></a>
            <form action="http://quorumventures.co.ke/" method="get"
        class="searchform ">
        <fieldset>
            <span class="text"><input name="s" id="s" type="text" value="" placeholder="Search&hellip;" autocomplete="off" /></span>
                        <span class="button-wrap"><button class="btn btn-special" title="Search" type="submit"><i class="fa fa-search"></i></button></span>
        </fieldset>
    </form>
        </div>
                        </div>

                                    </div>
    
                
            </div>
        </div>
        
<div id="nav-panel" class="">
    <div class="container">
        <div class="mobile-nav-wrap">
            <div class="menu-wrap"><ul id="menu-main-menu-1" class="mobile-menu accordion-menu"><li id="accordion-menu-item-541" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-143 current_page_item active"><a href="http://quorumventures.co.ke/" rel="nofollow" class=" current ">Home</a></li>
<li id="accordion-menu-item-597" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/about-us/" rel="nofollow" class="">About Us</a></li>
<li id="accordion-menu-item-879" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/our-portfolio/" rel="nofollow" class="">Our Portfolio</a></li>
<li id="accordion-menu-item-684" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/670-2/" rel="nofollow" class="">Blog</a></li>
<li id="accordion-menu-item-891" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="http://shop.quorumventures.co.ke/quotes/requestquote.php" rel="nofollow" class="">Enquire</a></li>
<li id="accordion-menu-item-892" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="http://shop.quorumventures.co.ke/quotes/makepayment.php" rel="nofollow" class="">Pay</a></li>
<li id="accordion-menu-item-791" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/contact/" rel="nofollow" class="">Contact Us</a></li>
</ul></div>        </div>
    </div>
</div>
    </div>
</header>
                            </div><!-- end header wrapper -->
