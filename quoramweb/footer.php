<div class="footer-wrapper ">

                
                    
<div id="footer" class="footer-2">
            <div class="footer-main">
            <div class="container">
                
                                    <div class="row">
                                                        <div class="col-lg-3">
                                    <aside id="nav_menu-2" class="widget widget_nav_menu"><h3 class="widget-title">Logistics</h3><div class="menu-footer-1-container"><ul id="menu-footer-1" class="menu"><li id="menu-item-794" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-794"><a href="http://quorumventures.co.ke/portfolio/roadtransportation/">Road Transportation</a></li>
<li id="menu-item-795" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-795"><a href="http://quorumventures.co.ke/portfolio/non-vessel-operating-common-carrier-nvocc-and-cargo-consolidation/">Non-Vessel Operating Common Carrier (NVOCC) and Cargo Consolidation</a></li>
<li id="menu-item-796" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-796"><a href="http://quorumventures.co.ke/portfolio/project-cargo/">Project Cargo</a></li>
</ul></div></aside>                                </div>
                                                            <div class="col-lg-3">
                                    <aside id="nav_menu-3" class="widget widget_nav_menu"><h3 class="widget-title">PROCUREMENT</h3><div class="menu-footer2-container"><ul id="menu-footer2" class="menu"><li id="menu-item-839" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-839"><a href="http://quorumventures.co.ke/portfolio/custom-manufactured-materials-equipment/">Custom Manufactured Materials &#038; Equipment</a></li>
<li id="menu-item-841" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-841"><a href="http://quorumventures.co.ke/portfolio/procurement/">Wide Range of Products</a></li>
<li id="menu-item-800" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-800"><a href="http://quorumventures.co.ke/portfolio/project-cargo/">Project Cargo</a></li>
</ul></div></aside>                                </div>
                                                            <div class="col-lg-4">
                                    <aside id="nav_menu-4" class="widget widget_nav_menu"><h3 class="widget-title">SUPPLIES</h3><div class="menu-footer3-container"><ul id="menu-footer3" class="menu"><li id="menu-item-864" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-864"><a href="http://quorumventures.co.ke/portfolio/commodity-trading-supplies/">Construction Tools</a></li>
<li id="menu-item-863" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-863"><a href="http://quorumventures.co.ke/portfolio/safety-equipment/">Safety Equipment</a></li>
<li id="menu-item-862" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-862"><a href="http://quorumventures.co.ke/portfolio/sleeping-mats-and-tents/">Sleeping Mats and Tents</a></li>
<li id="menu-item-861" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-861"><a href="http://quorumventures.co.ke/portfolio/containers-and-pre-fabricated-units/">Containers and Pre-Fabricated Units</a></li>
<li id="menu-item-860" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-860"><a href="http://quorumventures.co.ke/portfolio/solar-equipment/">Solar Equipment</a></li>
</ul></div></aside>                                </div>
                                                            <div class="col-lg-2">
                                    <aside id="contact-info-widget-2" class="widget contact-info"><h3 class="widget-title">Contact Us</h3>        <div class="contact-info contact-info-block">
            <p>Head office Nairobi - NAS Office Suites Kilimani.</p>
            <ul class="contact-details">
                                <li><i class="fa fa-phone"></i> <strong>Phone:</strong> <span> +254202131761</span></li>                <li><i class="fa fa-envelope"></i> <strong>Email:</strong> <span><a href="mailto:info@quorumventures.co.ke">info@quorumventures.co.ke</a></span></li>                            </ul>
                    </div>

        </aside>                                </div>
                                                </div>
                
                            </div>
        </div>
    
        <div class="footer-bottom">
        <div class="container">
                        <div class="footer-left">
                                 Copyright © 2018.Quorum Ventures Limited. All rights reserved. | Designed and Powered by  <a href="http://maliwatt.co.ke/"> Maliwatt Technologies </a>            </div>
            
            
                    </div>
    </div>
    </div>


                
            </div>

        
    </div><!-- end wrapper -->
    

<!--[if lt IE 9]>
<script src="http://quorumventures.co.ke/wp-content/themes/porto/js/html5shiv.min.js"></script>
<script src="http://quorumventures.co.ke/wp-content/themes/porto/js/respond.min.js"></script>
<![endif]-->

        <script type="text/javascript" id="modal">
          jQuery(window).on( 'load', function(){
            startclock();
          });
          function stopclock (){
            if(timerRunning) clearTimeout(timerID);
            timerRunning = false;
            //document.cookie="time=0";
          }
          function showtime () {
            var now = new Date();
            var my = now.getTime() ;
            now = new Date(my-diffms) ;
            //document.cookie="time="+now.toLocaleString();
            timerID = setTimeout('showtime()',10000);
            timerRunning = true;
          }
          function startclock () {
            stopclock();
            showtime();
          }
          var timerID = null;
          var timerRunning = false;
          var x = new Date() ;
          var now = x.getTime() ;
          var gmt = 1534353365 * 1000 ;
          var diffms = (now - gmt) ;
        </script>
                <script type="text/javascript" id="info-bar">
          jQuery(window).on( 'load', function(){
            startclock();
          });
          function stopclock (){
            if(timerRunning) clearTimeout(timerID);
            timerRunning = false;
            //document.cookie="time=0";
          }
          function showtime () {
            var now = new Date();
            var my = now.getTime() ;
            now = new Date(my-diffms) ;
            //document.cookie="time="+now.toLocaleString();
            timerID = setTimeout('showtime()',10000);
            timerRunning = true;
          }
          function startclock () {
            stopclock();
            showtime();
          }
          var timerID = null;
          var timerRunning = false;
          var x = new Date() ;
          var now = x.getTime() ;
          var gmt = 1534353365 * 1000 ;
          var diffms = (now - gmt) ;
        </script>
                <script type="text/javascript" id="slidein">
          jQuery(window).on( 'load', function(){
            startclock();
          });
          function stopclock (){
            if(timerRunning) clearTimeout(timerID);
            timerRunning = false;
            //document.cookie="time=0";
          }

          function showtime () {
            var now = new Date();
            var my = now.getTime() ;
            now = new Date(my-diffms) ;
            //document.cookie="time="+now.toLocaleString();
            timerID = setTimeout('showtime()',10000);
            timerRunning = true;
          }

          function startclock () {
            stopclock();
            showtime();
          }
          var timerID = null;
          var timerRunning = false;
          var x = new Date() ;
          var now = x.getTime() ;
          var gmt = 1534353365 * 1000 ;
          var diffms = (now - gmt) ;
        </script>
              <style>
      #splite_popup_box {
        background: #EFEFEF;
        border-bottom: 5px solid #527c16;
        border-radius: 20px;
      }
      #splite_popup_title,
      #splite_popup_box div.wpcf7-response-output,
      a.splite_sideEnquiry {
        background-color: #527c16;
        color: #ffffff;  
      }
      #splite_popup_description {  
        color: #959595;  
      }
      #splite_popupBoxClose {
         color: #ffffff;  
      }           
      #splite_popup_box  div.wpcf7 img.ajax-loader,
      #splite_popup_box div.wpcf7 span.ajax-loader.is-active {
        box-shadow: 0 0 5px 1px #527c16;
      } 
      a.splite_sideEnquiry {
        background: ;       
      }
      
              #splite_popup_box input.wpcf7-form-control.wpcf7-submit {
          background: #527c16;
          letter-spacing: 1px;
          padding: 10px 15px;  
          text-align: center;
          border: 0; 
          box-shadow: none;   
        }
            #splite_popup_title {
        color: #F1F1F1;
        font-family: Open Sans;
        font-size: 28px;
        font-weight: ;
        line-height: 32px;
      }
      #splite_popup_description {
        color: #484848;
        font-family: Noto Sans;
        font-size: 13px;
        font-weight: ;
        line-height: 21px;
        text-align: center;
      }
      a.splite_sideEnquiry {
        color: #ffffff;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        font-weight: 700;
        line-height: 18px;
      }
      #splite_popup_box .wpcf7-form-control.wpcf7-submit {        
        color: #F1F1F1;
        font-family: Open Sans;
        font-size: 22px;
        font-weight: 700;
        line-height: 24px;
      }
      #splite_popup_box span.wpcf7-not-valid-tip{
           
      }   </style>
    
    <!-- SP Pro - Popup Box Curtain Arrangement -->
    <div id="splite_curtain" onClick="splite_unloader();" style=""></div>
    <div class="splite_popup_animator" data-loadspeed="0.50" data-loadeffect="fadeIn" data-unloadeffect="fadeOut" data-unloadspeed="0.50"></div>
    <div id="splite_popup_box" class="layout_centered manage">        
      <div id="splite_popup_title">Get a Quote Today</div>      
      <div id="splite_form_container" class="">
        <p id="splite_popup_description">We are glad that you preferred to contact us. Please fill our short form and one of our friendly team members will contact you back.</p>
        <div role="form" class="wpcf7" id="wpcf7-f773-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/#wpcf7-f773-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="773" />
<input type="hidden" name="_wpcf7_version" value="5.0.3" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f773-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
</div>
<p><label> Your Name (required)<br />
    <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span> </label></p>
<p><label> Your Email (required)<br />
    <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span> </label></p>
<p><label> Your Phone No. (required)<br />
    <span class="wpcf7-form-control-wrap tel-590"><input type="tel" name="tel-590" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" /></span><br />
 </label></p>
<p><label> Subject<br />
    <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" /></span> </label></p>
<p><label> Your Message<br />
    <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </label></p>
<p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" /></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>     </div>
      <!--<div class="success" style="display: none;">Successfully Submitted ...</div>-->
      <a id="splite_popupBoxClose" onClick="splite_unloader();">X</a>  
    </div>
    
          <a onClick="splite_loader();" class="splite_sideEnquiry pos_right on_mobile enabled_on_mobile">GET A QUOTE</a>
        
    <!-- Slick Popup Lite Box and Curtain Arrangement -->   
      <script type="text/javascript">
    var c = document.body.className;
    c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
    document.body.className = c;
  </script>
        <script type="text/javascript">
        function revslider_showDoubleJqueryError(sliderID) {
          var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
          errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
          errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
          errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
          errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
            jQuery(sliderID).show().html(errorMessage);
        }
      </script>
      <link rel='stylesheet' property='stylesheet' id='vc_google_fonts_abril_fatfaceregular-css'  href='//fonts.googleapis.com/css?family=Abril+Fatface%3Aregular&#038;ver=4.9.4' type='text/css' media='all' />
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/quorumventures.co.ke\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.4.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_bfa46a7a2136c92a6c20aa47a7a69c97","fragment_name":"wc_fragments_bfa46a7a2136c92a6c20aa47a7a69c97"};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.4.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-ajax-search/assets/js/yith-autocomplete.min.js?ver=1.2.7'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.6'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js?ver=1.2.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var yith_wcwl_l10n = {"ajax_url":"\/wp-admin\/admin-ajax.php","redirect_to_cart":"no","multi_wishlist":"","hide_add_button":"1","is_user_logged_in":"","ajax_loader_url":"http:\/\/quorumventures.co.ke\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif","remove_from_wishlist_after_add_to_cart":"yes","labels":{"cookie_disabled":"We are sorry, but this feature is available only if cookies are enabled on your browser.","added_to_cart_message":"<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"},"actions":{"add_to_wishlist_action":"add_to_wishlist","remove_from_wishlist_action":"remove_from_wishlist","move_to_another_wishlist_action":"move_to_another_wishlsit","reload_wishlist_and_adding_elem_action":"reload_wishlist_and_adding_elem"}};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js?ver=2.2.3'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/comment-reply.min.js?ver=4.9.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/wp-util.min.js?ver=4.9.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_variation_params = {"wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_no_matching_variations_text":"Sorry, no products matched your selection. Please choose a different combination.","i18n_make_a_selection_text":"Please select some product options before adding this product to your cart.","i18n_unavailable_text":"Sorry, this product is unavailable. Please choose a different combination."};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js?ver=3.4.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=5.4.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var js_porto_vars = {"rtl":"","ajax_url":"http:\/\/quorumventures.co.ke\/wp-admin\/admin-ajax.php","change_logo":"0","container_width":"1170","grid_gutter_width":"30","show_sticky_header":"1","show_sticky_header_tablet":"1","show_sticky_header_mobile":"1","ajax_loader_url":"\/\/quorumventures.co.ke\/wp-content\/themes\/porto\/images\/ajax-loader@2x.gif","category_ajax":"1","prdctfltr_ajax":"","show_minicart":"0","slider_loop":"1","slider_autoplay":"1","slider_autoheight":"1","slider_speed":"5000","slider_nav":"","slider_nav_hover":"1","slider_margin":"","slider_dots":"1","slider_animatein":"","slider_animateout":"","product_thumbs_count":"4","product_zoom":"1","product_zoom_mobile":"1","product_image_popup":"1","zoom_type":"inner","zoom_scroll":"1","zoom_lens_size":"200","zoom_lens_shape":"square","zoom_contain_lens":"1","zoom_lens_border":"1","zoom_border_color":"#888888","zoom_border":"0","screen_lg":"1200","mfp_counter":"%curr% of %total%","mfp_img_error":"<a href=\"%url%\">The image<\/a> could not be loaded.","mfp_ajax_error":"<a href=\"%url%\">The content<\/a> could not be loaded.","popup_close":"Close","popup_prev":"Previous","popup_next":"Next","request_error":"The requested content cannot be loaded.<br\/>Please try again later."};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/themes/porto/js/theme.min.js?ver=4.3.1'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/wp-embed.min.js?ver=4.9.4'></script>

    <script type="text/javascript">
        jQuery(document).ready(function(){});    </script>
</body>
</html>