<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="http://quorumventures.co.ke/xmlrpc.php" />

        <link rel="shortcut icon" href="//quorumventures.co.ke/wp-content/uploads/2018/08/favicon.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="//quorumventures.co.ke/wp-content/uploads/2018/08/favicon.png">
    <link rel="apple-touch-icon" sizes="120x120" href="//quorumventures.co.ke/wp-content/uploads/2018/08/favicon.png">
    <link rel="apple-touch-icon" sizes="76x76" href="//quorumventures.co.ke/wp-content/uploads/2018/08/favicon.png">
    <link rel="apple-touch-icon" sizes="152x152" href="//quorumventures.co.ke/wp-content/uploads/2018/08/favicon.png">

        <script type="text/javascript">document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>
      <title>Quorum Ventures &#8211; Building a firm business network</title>
      <style>
        .wishlist_table .add_to_cart, a.add_to_wishlist.button.alt { border-radius: 16px; -moz-border-radius: 16px; -webkit-border-radius: 16px; }      </style>
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Quorum Ventures &raquo; Feed" href="http://quorumventures.co.ke/feed/" />
<link rel="alternate" type="application/rss+xml" title="Quorum Ventures &raquo; Comments Feed" href="http://quorumventures.co.ke/comments/feed/" />
    <script type="text/javascript">
      window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/quorumventures.co.ke\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.4"}};
      !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
img.wp-smiley,
img.emoji {
  display: inline !important;
  border: none !important;
  box-shadow: none !important;
  height: 1em !important;
  width: 1em !important;
  margin: 0 .07em !important;
  vertical-align: -0.1em !important;
  background: none !important;
  padding: 0 !important;
}
</style>
<link rel='stylesheet' id='validate-engine-css-css'  href='http://quorumventures.co.ke/wp-content/plugins/wysija-newsletters/css/validationEngine.jquery.css?ver=2.9' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://quorumventures.co.ke/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='http://quorumventures.co.ke/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.7.1' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
.tparrows:before{color:#83bfd6;text-shadow:0 0 3px #fff;}.revslider-initialised .tp-loader{z-index:18;}
</style>
<style id='woocommerce-inline-inline-css' type='text/css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel='stylesheet' id='yith_wcas_frontend-css'  href='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-ajax-search/assets/css/yith_wcas_ajax_search.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/css/prettyPhoto.css?ver=3.4.4' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-selectBox-css'  href='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=1.2.0' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-main-css'  href='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-wishlist/assets/css/style.css?ver=2.2.3' type='text/css' media='all' />
<link rel='stylesheet' id='instag-slider-css'  href='http://quorumventures.co.ke/wp-content/plugins/instagram-slider-widget/assets/css/instag-slider.css?ver=1.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='splite-css-css'  href='http://quorumventures.co.ke/wp-content/plugins/slick-popup/libs/css/styles.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='splite-animate-css'  href='http://quorumventures.co.ke/wp-content/plugins/slick-popup/libs/css/animate.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='http://quorumventures.co.ke/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.4.7' type='text/css' media='all' />
<link rel='stylesheet' id='redux-google-fonts-splite_opts-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A700%7CNoto+Sans&#038;ver=1533834491' type='text/css' media='all' />
<link rel='stylesheet' id='porto-bootstrap-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/bootstrap_1.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-plugins-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/plugins.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-google-fonts-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A200%2C300%2C400%2C700%2C800%2C600%7CShadows+Into+Light%3A200%2C300%2C400%2C700%2C800%2C600%7C&#038;subset=cyrillic%2Ccyrillic-ext%2Cgreek%2Cgreek-ext%2Ckhmer%2Clatin%2Clatin-ext%2Cvietnamese&#038;ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-theme-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/theme.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-theme-shop-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/theme_shop.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-dynamic-style-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/dynamic_style_1.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-skin-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/skin_1.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-style-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/style.css?ver=4.9.4' type='text/css' media='all' />
<!--[if lt IE 10]>
<link rel='stylesheet' id='porto-ie-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/ie.css?ver=4.9.4' type='text/css' media='all' />
<![endif]-->
<script type="text/template" id="tmpl-variation-template">
  <div class="woocommerce-variation-description">{{{ data.variation.variation_description }}}</div>
  <div class="woocommerce-variation-price">{{{ data.variation.price_html }}}</div>
  <div class="woocommerce-variation-availability">{{{ data.variation.availability_html }}}</div>
</script>
<script type="text/template" id="tmpl-unavailable-variation-template">
  <p>Sorry, this product is unavailable. Please choose a different combination.</p>
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var slide_in = {"demo_dir":"http:\/\/quorumventures.co.ke\/wp-content\/plugins\/convertplug\/modules\/slide_in\/assets\/demos"};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.7.1'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.7.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/quorumventures.co.ke\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.4.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js?ver=5.4.7'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/instagram-slider-widget/assets/js/jquery.flexslider-min.js?ver=2.2'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/slick-popup/libs/js/jquery.nicescroll.min.js?ver=4.9.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/slick-popup/libs/js/custom.js?ver=4.9.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/themes/porto/js/popper.min.js?ver=4.3.1'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/themes/porto/js/bootstrap.min.js?ver=4.3.1'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/themes/porto/js/plugins.min.js?ver=4.3.1'></script>
<link rel='https://api.w.org/' href='http://quorumventures.co.ke/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://quorumventures.co.ke/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://quorumventures.co.ke/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.4" />
<meta name="generator" content="WooCommerce 3.4.4" />
<link rel="canonical" href="http://quorumventures.co.ke/" />
<link rel='shortlink' href='http://quorumventures.co.ke/' />
<link rel="alternate" type="application/json+oembed" href="http://quorumventures.co.ke/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fquorumventures.co.ke%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://quorumventures.co.ke/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fquorumventures.co.ke%2F&#038;format=xml" />

<style id="porto-generated-css-output" type="text/css">.ms-loading-container .ms-loading,.ms-slide .ms-slide-loading{background-image:none !important;background-color:transparent !important;box-shadow:none !important;}#header .logo{max-width:170px;}@media (min-width:1200px){#header .logo{max-width:250px;}}@media (max-width:991px){#header .logo{max-width:110px;}}@media (max-width:767px){#header .logo{max-width:110px;}}.sale-product-daily-deal .daily-deal-title,.sale-product-daily-deal .porto_countdown{font-family:'Oswald',Open Sans,Open Sans;text-transform:uppercase;}.entry-summary .sale-product-daily-deal{margin-top:10px;}.entry-summary .sale-product-daily-deal .porto_countdown{margin-bottom:5px;}.entry-summary .sale-product-daily-deal .porto_countdown-section{background-color:#83bfd6;color:#fff;margin-left:1px;margin-right:1px;display:block;float:left;max-width:calc(25% - 2px);min-width:64px;padding:12px 10px;}.entry-summary .sale-product-daily-deal .porto_countdown .porto_countdown-amount{display:block;font-size:18px;font-weight:700;}.entry-summary .sale-product-daily-deal .porto_countdown-period{font-size:10px;}.entry-summary .sale-product-daily-deal:after{content:'';display:table;clear:both;}.entry-summary .sale-product-daily-deal .daily-deal-title{text-transform:uppercase;}.products .sale-product-daily-deal{position:absolute;left:10px;right:10px;bottom:10px;color:#fff;padding:5px 0;text-align:center;}.products .sale-product-daily-deal:before{content:'';position:absolute;left:0;width:100%;top:0;height:100%;background:#83bfd6;opacity:0.7;}.products .sale-product-daily-deal > h5,.products .sale-product-daily-deal > div{position:relative;z-index:1;}.products .sale-product-daily-deal .daily-deal-title{display:inline-block;color:#fff;font-size:11px;font-weight:400;margin-bottom:0;margin-right:1px;}.products .sale-product-daily-deal .porto_countdown{float:none;display:inline-block;text-transform:uppercase;margin-bottom:0;width:auto;}.products .sale-product-daily-deal .porto_countdown-section{padding:0;margin-bottom:0;}.products .sale-product-daily-deal .porto_countdown-section:first-child:after{content:',';margin-right:2px;}.products .sale-product-daily-deal .porto_countdown-amount,.products .sale-product-daily-deal .porto_countdown-period{font-size:13px;font-weight:500;padding:0 1px;}.products .sale-product-daily-deal .porto_countdown-section:last-child .porto_countdown-period{padding:0;}.products .sale-product-daily-deal:after{content:'';display:table;clear:both;}@media (min-width:992px){#header .header-main .container .header-left{padding:20px 0;}}#header.sticky-header .header-main .container .header-left{padding:10px 0;}</style>  <noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
  <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://quorumventures.co.ke/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.7.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<script type="text/javascript">function setREVStartSize(e){                 
            try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
              if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})          
            }catch(d){console.log("Failure at Presize of Slider:"+d)}           
          };</script>
<style type="text/css" title="dynamic-css" class="options-output">{color:#527c16;}{color:#EFEFEF;}{color:#ffffff;}</style><style type="text/css" data-type="vc_custom-css">#our-button{
    background :#527C16;
}</style><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1534265455682{margin-right: 5px !important;background-color: #1e73be !important;border-radius: 5px !important;}.vc_custom_1534255394105{margin-left: 5px !important;background-color: #81d742 !important;border-radius: 5px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
<script src="../assets/js/jsurl.js"></script>

  <style type="text/css">

    .greenboder
    {
      border: 1px;
      border-color: #527C16;
      border-style: solid;;
    }
    .greenbackground
    {
      background-color:#527C16;
      color:white; 
    }

    .navbar-default .navbar-nav > .active{
    color: #000;
    background: #d65c14;
}
.navbar-inverse .navbar-nav > .active > a,
.navbar-inverse .navbar-nav > .active > a:hover,
.navbar-inverse .navbar-nav > .active > a:focus {
  color: black;
  background-color: white;
  border-radius: 10px;


}
.loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
#loading-indicator {
  position: relative;
  width: 80px;
  height: 80px;
   margin: 0 auto;
  
}
.error
{
  color:red;
}
.modal-confirm {    
    color: #636363;
    width: 325px;
  }
  .modal-confirm .modal-content {
    padding: 20px;
    border-radius: 5px;
    border: none;
  }
  .modal-confirm .modal-header {
    border-bottom: none;   
        position: relative;
  }
  .modal-confirm h4 {
    text-align: center;
    font-size: 26px;
    margin: 30px 0 -15px;
  }
  .modal-confirm .form-control, .modal-confirm .btn {
    min-height: 40px;
    border-radius: 3px; 
  }
  .modal-confirm .close {
        position: absolute;
    top: -5px;
    right: -5px;
  } 
  .modal-confirm .modal-footer {
    border: none;
    text-align: center;
    border-radius: 5px;
    font-size: 13px;
  } 
  .modal-confirm .icon-box {
    color: #fff;    
    position: absolute;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: -70px;
    width: 95px;
    height: 95px;
    border-radius: 50%;
    z-index: 9;
    background: #82ce34;
    padding: 15px;
    text-align: center;
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
  }
  .modal-confirm .icon-box i {
    font-size: 58px;
    position: relative;
    top: 3px;
  }
  .modal-confirm.modal-dialog {
    margin-top: 80px;
  }
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
    background: #82ce34;
    text-decoration: none;
    transition: all 0.4s;
        line-height: normal;
        border: none;
    }
  .modal-confirm .btn:hover, .modal-confirm .btn:focus {
    background: #6fb32b;
    outline: none;
  }
  .trigger-btn {
    display: inline-block;
    margin: 100px auto;
  }
  </style>
   <script>
            $(document).ready(function () {
                $('#loading-indicator').hide();
                
                 $('#request_form').submit(function (e) {
                   
                    e.preventDefault();
                     $('#loading-indicator').show();
                     $clientname= $('#client_name').val();
                     $clientcompany= $('#client_company').val();
                     $clienttopic= $('#client_topic').val();
                     $clientdetails=$('#client_details').val();
                     $clientemail=$('#client_email').val();

                       var error="";
                       var errormsg="";
                       if($clientname=="")
                       {
                         
                         document.getElementById("nameerror").innerHTML ="Name is required";
                         error="true";
                       }
                       if($clientcompany=="")
                       {
                         
                         document.getElementById("companyerror").innerHTML ="Your company name is required";
                         error="true";
                       }
                       if($clienttopic=="")
                       {
                         
                          document.getElementById("topicerror").innerHTML ="Title Of your quote is Required";
                         error="true";
                       }
                         if($clientdetails=="")
                       {
                         
                         document.getElementById("detailserror").innerHTML ="Details Of your Quote are required";
                         error="true";  
                       }
                         if($clientemail=="")
                       {
            
                          document.getElementById("emailerror").innerHTML ="Email is required";
                         error="true";
                       }
                     
                 if(error=="true")
                 {
                  $('#loading-indicator').hide();
                   console.log(error);
                 }
                 else
                 {    document.getElementById("nameerror").innerHTML ="";
                      document.getElementById("companyerror").innerHTML ="";
                      document.getElementById("topicerror").innerHTML ="";
                      document.getElementById("detailserror").innerHTML ="";
                      document.getElementById("emailerror").innerHTML ="";
                      $("#submit").addClass("disabled");
                        $(".status-progress-add").show();
                        console.log(new FormData(this));
                        $.ajax({
                        url: base_url2 + "Quoterequest/requestq",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $('#loading-indicator').hide();
                            $('#loading-indicator').hide();
                             $("#client_company").val("");
                             $("#client_name").val("");
                             $("#client_topic").val("");
                             $("#client_details").val("");
                             $("#client_email").val("");
                              $("#myModal1").modal('show');

                        },
                        error: function () {}
                        });
                   
                 }
});
                 validate = function(vari)
                 {
                     if(vari=='cname'&& $('#client_company').val()=="")
                     {
                        document.getElementById("companyerror").innerHTML ="company name is required";
                     }
                     else if(vari=='cname'&& $('#client_company').val()!="")
                     {
                          document.getElementById("companyerror").innerHTML ="";
                     }

                     if(vari=='name'&& $('#client_name').val()=="")
                     {
                        document.getElementById("nameerror").innerHTML ="Your name is required";
                     }
                     else if(vari=='cname'&& $('#client_company').val()!="")
                     {
                       document.getElementById("nameerror").innerHTML ="";
                     }
                     if(vari=='email'&& $('#client_email').val()=="")
                     {
                        document.getElementById("emailerror").innerHTML ="Email is required";
                     }
                     else if(vari=='email'&& $('#client_company').val()!="")
                     {
                        document.getElementById("emailerror").innerHTML ="";
                     }
                     if(vari=='top'&& $('#client_topic').val()=="")
                     {
                        document.getElementById("topicerror").innerHTML ="Topic is required";
                     }
                     else if(vari=='top'&& $('#client_topic').val()!="")
                     {
                        document.getElementById("topicerror").innerHTML ="";
                     }
                     if(vari=='details'&& $('#client_details').val()=="")
                     {
                        document.getElementById("detailserror").innerHTML ="Details required";
                     }
                     else if(vari=='details'&& $('#client_details').val()!="")
                     {
                       document.getElementById("detailserror").innerHTML ="";
                     }
                 }
$('#example').DataTable();
});

            
            
        </script>
</head>
<body class="home page-template-default page page-id-143 full blog-1  woocommerce-no-js yith-wcan-free woocommerce-wishlist woocommerce woocommerce-page wpb-js-composer js-comp-ver-5.4.7 vc_responsive">
    
    <div class="page-wrapper"><!-- page wrapper -->

        
                    <div class="header-wrapper clearfix"><!-- header wrapper -->
                                
                    <header id="header" class="header-8 search-sm">
    
    <div class="header-main">
        <div class="container">
            <div class="header-left">
                <h1 class="logo">    <a href="http://quorumventures.co.ke/" title="Quorum Ventures - Building a firm business network" rel="home">
                <img class="img-responsive standard-logo" src="//quorumventures.co.ke/wp-content/uploads/2018/08/Quroum-logo.jpeg" alt="Quorum Ventures" /><img class="img-responsive retina-logo" src="//quorumventures.co.ke/wp-content/uploads/2018/08/Quroum-logo.jpeg" alt="Quorum Ventures" style="display:none;" />            </a>
    </h1>            </div>
            <div class="header-center">
                <div id="main-menu">
                    <ul id="menu-main-menu" class="main-menu mega-menu show-arrow effect-down subeffect-fadein-left"><li id="nav-menu-item-541" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-143 current_page_item active narrow "><a href="http://quorumventures.co.ke/" class=" current">Home</a></li>
<li id="nav-menu-item-597" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/about-us/" class="">About Us</a></li>
<li id="nav-menu-item-879" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/our-portfolio/" class="">Our Portfolio</a></li>
<li id="nav-menu-item-684" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/670-2/" class="">Blog</a></li>
<li id="nav-menu-item-891" class="menu-item menu-item-type-custom menu-item-object-custom  narrow "><a href="http://shop.quorumventures.co.ke/quotes/requestquote.php" class="">Enquire</a></li>
<li id="nav-menu-item-892" class="menu-item menu-item-type-custom menu-item-object-custom  narrow "><a href="http://shop.quorumventures.co.ke/quotes/makepayment.php" class="">Pay</a></li>
<li id="nav-menu-item-791" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/contact/" class="">Contact Us</a></li>
</ul>                </div>
            </div>
            <div class="header-right search-popup">
                                <div class="">
                                        <a class="mobile-toggle"><i class="fa fa-reorder"></i></a>
                    <div class="block-nowrap">
                            <div class="searchform-popup">
        <a class="search-toggle"><i class="fa fa-search"></i></a>
            <form action="http://quorumventures.co.ke/" method="get"
        class="searchform ">
        <fieldset>
            <span class="text"><input name="s" id="s" type="text" value="" placeholder="Search&hellip;" autocomplete="off" /></span>
                        <span class="button-wrap"><button class="btn btn-special" title="Search" type="submit"><i class="fa fa-search"></i></button></span>
        </fieldset>
    </form>
        </div>
                        </div>

                                    </div>
    
                
            </div>
        </div>
        
<div id="nav-panel" class="">
    <div class="container">
        <div class="mobile-nav-wrap">
            <div class="menu-wrap"><ul id="menu-main-menu-1" class="mobile-menu accordion-menu"><li id="accordion-menu-item-541" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-143 current_page_item active"><a href="http://quorumventures.co.ke/" rel="nofollow" class=" current ">Home</a></li>
<li id="accordion-menu-item-597" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/about-us/" rel="nofollow" class="">About Us</a></li>
<li id="accordion-menu-item-879" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/our-portfolio/" rel="nofollow" class="">Our Portfolio</a></li>
<li id="accordion-menu-item-684" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/670-2/" rel="nofollow" class="">Blog</a></li>
<li id="accordion-menu-item-891" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="http://shop.quorumventures.co.ke/quotes/requestquote.php" rel="nofollow" class="">Enquire</a></li>
<li id="accordion-menu-item-892" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="http://shop.quorumventures.co.ke/quotes/makepayment.php" rel="nofollow" class="">Pay</a></li>
<li id="accordion-menu-item-791" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/contact/" rel="nofollow" class="">Contact Us</a></li>
</ul></div>        </div>
    </div>
</div>
    </div>
</header>
</div><!-- end header wrapper -->