<?php
include('../header.php');
 ?> 
<div class="container">
 
 
  <div class="row">
 
 	<div class="col-md-10 col-md-offset-1" >
 		
 		<p style="float:left; font-size: 150%;">
           <span style="color:#527C16; float:left;">Get A Quote</span>
             <hr style="height:5px;border:none;color:#527C16;background-color:#527C16;"/>
        </p>
        <p style="color:#527C16;">We deliver Promises ...</p>
 	</div>
 	
 </div>
   <div class="row">
 
 	<div class="col-md-10 col-md-offset-1" >
 		
 	     <div class="row">
 	     	<div class="col-md-7">
 	     		<div class="panel panel-default">
      <div class="panel-heading"><p>Fill in the required information.</p><br><p>Fields marked with (*) are mandatory</p></div>
      <div class="panel-body">
      	
        <img src="../assets/img/loading.gif" id="loading-indicator" style="display:none">

      	<form name="request_form" id ="request_form" >
      		<span id="companyerror" class="error"></span>
      	<input type="text" name="client_company" id="client_company" class="form-control greenboder" placeholder="Your Company Name *"  onkeyup="validate('cname')"></br>
      	                            <span id="nameerror" class="error"></span>
									<input type="text" name="client_name" id="client_name" class="form-control greenboder" placeholder="Your Name *" onkeyup="validate('name')"></br>
									<span id="emailerror" class="error"></span>
									<input type="email" name="client_email" id="client_email" class="form-control greenboder" placeholder="email *" onkeyup="validate('email')"></br>
									<span id="topicerror" class="error"></span>
									<input type="text" name="client_topic" id="client_topic" class="form-control greenboder" placeholder="Enquiry Topic *" onkeyup="validate('topic')"></br>
									<span id="detailserror" class="error"></span>
									<textarea class="form-control greenboder" id="client_details" name="client_details" placeholder="Quote Details *" onkeyup="validate('details')"></textarea></br>
								
      </div>
      <div class="modal-footer">
       
        <button type="submit" class="form-control btn btn-large  greenbackground">Submit</button>
                                  </form></div>
    </div>
<div style="text-align: right; text-decoration: underline;"><a href="http://shop.quorumventures.co.ke/quotes/makepayment.php" > Make A Payment </a> </div>
 	     	</div>
 	     	<div class="col-md-5"  align="center">
 	     		
 	     			<p style="background-color:#527C16; margin: 0 auto; color: white;">
QUORUM VENTURES Head office<br>
Nairobi - NAS Office Suites Kilimani<br>
Tel: +254 202131761<br>
Email: info@quorumventures.co.ke<br>
www.quorumventures.co.ke<br></p>    		
 	     	</div>
 	     </div>
 	</div>
 	
 </div>
</div>

</body>
</html>
