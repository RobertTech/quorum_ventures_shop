
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="http://quorumventures.co.ke/xmlrpc.php" />

        <link rel="shortcut icon" href="//quorumventures.co.ke/wp-content/uploads/2018/08/favicon.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="//quorumventures.co.ke/wp-content/uploads/2018/08/favicon.png">
    <link rel="apple-touch-icon" sizes="120x120" href="//quorumventures.co.ke/wp-content/uploads/2018/08/favicon.png">
    <link rel="apple-touch-icon" sizes="76x76" href="//quorumventures.co.ke/wp-content/uploads/2018/08/favicon.png">
    <link rel="apple-touch-icon" sizes="152x152" href="//quorumventures.co.ke/wp-content/uploads/2018/08/favicon.png">

        <script type="text/javascript">document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>
      <title>Quorum Ventures &#8211; Building a firm business network</title>
      <style>
        .wishlist_table .add_to_cart, a.add_to_wishlist.button.alt { border-radius: 16px; -moz-border-radius: 16px; -webkit-border-radius: 16px; }      </style>
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Quorum Ventures &raquo; Feed" href="http://quorumventures.co.ke/feed/" />
<link rel="alternate" type="application/rss+xml" title="Quorum Ventures &raquo; Comments Feed" href="http://quorumventures.co.ke/comments/feed/" />
    <script type="text/javascript">
      window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/quorumventures.co.ke\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.4"}};
      !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
img.wp-smiley,
img.emoji {
  display: inline !important;
  border: none !important;
  box-shadow: none !important;
  height: 1em !important;
  width: 1em !important;
  margin: 0 .07em !important;
  vertical-align: -0.1em !important;
  background: none !important;
  padding: 0 !important;
}
</style>
<link rel='stylesheet' id='validate-engine-css-css'  href='http://quorumventures.co.ke/wp-content/plugins/wysija-newsletters/css/validationEngine.jquery.css?ver=2.9' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://quorumventures.co.ke/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='http://quorumventures.co.ke/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.7.1' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
.tparrows:before{color:#83bfd6;text-shadow:0 0 3px #fff;}.revslider-initialised .tp-loader{z-index:18;}
</style>
<style id='woocommerce-inline-inline-css' type='text/css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel='stylesheet' id='yith_wcas_frontend-css'  href='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-ajax-search/assets/css/yith_wcas_ajax_search.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/css/prettyPhoto.css?ver=3.4.4' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-selectBox-css'  href='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=1.2.0' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-main-css'  href='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-wishlist/assets/css/style.css?ver=2.2.3' type='text/css' media='all' />
<link rel='stylesheet' id='instag-slider-css'  href='http://quorumventures.co.ke/wp-content/plugins/instagram-slider-widget/assets/css/instag-slider.css?ver=1.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='splite-css-css'  href='http://quorumventures.co.ke/wp-content/plugins/slick-popup/libs/css/styles.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='splite-animate-css'  href='http://quorumventures.co.ke/wp-content/plugins/slick-popup/libs/css/animate.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='http://quorumventures.co.ke/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.4.7' type='text/css' media='all' />
<link rel='stylesheet' id='redux-google-fonts-splite_opts-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A700%7CNoto+Sans&#038;ver=1533834491' type='text/css' media='all' />
<link rel='stylesheet' id='porto-bootstrap-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/bootstrap_1.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-plugins-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/plugins.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-google-fonts-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A200%2C300%2C400%2C700%2C800%2C600%7CShadows+Into+Light%3A200%2C300%2C400%2C700%2C800%2C600%7C&#038;subset=cyrillic%2Ccyrillic-ext%2Cgreek%2Cgreek-ext%2Ckhmer%2Clatin%2Clatin-ext%2Cvietnamese&#038;ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-theme-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/theme.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-theme-shop-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/theme_shop.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-dynamic-style-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/dynamic_style_1.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-skin-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/skin_1.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='porto-style-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/style.css?ver=4.9.4' type='text/css' media='all' />
<!--[if lt IE 10]>
<link rel='stylesheet' id='porto-ie-css'  href='http://quorumventures.co.ke/wp-content/themes/porto/css/ie.css?ver=4.9.4' type='text/css' media='all' />
<![endif]-->
<script type="text/template" id="tmpl-variation-template">
  <div class="woocommerce-variation-description">{{{ data.variation.variation_description }}}</div>
  <div class="woocommerce-variation-price">{{{ data.variation.price_html }}}</div>
  <div class="woocommerce-variation-availability">{{{ data.variation.availability_html }}}</div>
</script>
<script type="text/template" id="tmpl-unavailable-variation-template">
  <p>Sorry, this product is unavailable. Please choose a different combination.</p>
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var slide_in = {"demo_dir":"http:\/\/quorumventures.co.ke\/wp-content\/plugins\/convertplug\/modules\/slide_in\/assets\/demos"};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.7.1'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.7.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/quorumventures.co.ke\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.4.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js?ver=5.4.7'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/instagram-slider-widget/assets/js/jquery.flexslider-min.js?ver=2.2'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/slick-popup/libs/js/jquery.nicescroll.min.js?ver=4.9.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/slick-popup/libs/js/custom.js?ver=4.9.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/themes/porto/js/popper.min.js?ver=4.3.1'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/themes/porto/js/bootstrap.min.js?ver=4.3.1'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/themes/porto/js/plugins.min.js?ver=4.3.1'></script>
<link rel='https://api.w.org/' href='http://quorumventures.co.ke/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://quorumventures.co.ke/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://quorumventures.co.ke/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.4" />
<meta name="generator" content="WooCommerce 3.4.4" />
<link rel="canonical" href="http://quorumventures.co.ke/" />
<link rel='shortlink' href='http://quorumventures.co.ke/' />
<link rel="alternate" type="application/json+oembed" href="http://quorumventures.co.ke/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fquorumventures.co.ke%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://quorumventures.co.ke/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fquorumventures.co.ke%2F&#038;format=xml" />
<style id="porto-generated-css-output" type="text/css">.ms-loading-container .ms-loading,.ms-slide .ms-slide-loading{background-image:none !important;background-color:transparent !important;box-shadow:none !important;}#header .logo{max-width:170px;}@media (min-width:1200px){#header .logo{max-width:250px;}}@media (max-width:991px){#header .logo{max-width:110px;}}@media (max-width:767px){#header .logo{max-width:110px;}}.sale-product-daily-deal .daily-deal-title,.sale-product-daily-deal .porto_countdown{font-family:'Oswald',Open Sans,Open Sans;text-transform:uppercase;}.entry-summary .sale-product-daily-deal{margin-top:10px;}.entry-summary .sale-product-daily-deal .porto_countdown{margin-bottom:5px;}.entry-summary .sale-product-daily-deal .porto_countdown-section{background-color:#83bfd6;color:#fff;margin-left:1px;margin-right:1px;display:block;float:left;max-width:calc(25% - 2px);min-width:64px;padding:12px 10px;}.entry-summary .sale-product-daily-deal .porto_countdown .porto_countdown-amount{display:block;font-size:18px;font-weight:700;}.entry-summary .sale-product-daily-deal .porto_countdown-period{font-size:10px;}.entry-summary .sale-product-daily-deal:after{content:'';display:table;clear:both;}.entry-summary .sale-product-daily-deal .daily-deal-title{text-transform:uppercase;}.products .sale-product-daily-deal{position:absolute;left:10px;right:10px;bottom:10px;color:#fff;padding:5px 0;text-align:center;}.products .sale-product-daily-deal:before{content:'';position:absolute;left:0;width:100%;top:0;height:100%;background:#83bfd6;opacity:0.7;}.products .sale-product-daily-deal > h5,.products .sale-product-daily-deal > div{position:relative;z-index:1;}.products .sale-product-daily-deal .daily-deal-title{display:inline-block;color:#fff;font-size:11px;font-weight:400;margin-bottom:0;margin-right:1px;}.products .sale-product-daily-deal .porto_countdown{float:none;display:inline-block;text-transform:uppercase;margin-bottom:0;width:auto;}.products .sale-product-daily-deal .porto_countdown-section{padding:0;margin-bottom:0;}.products .sale-product-daily-deal .porto_countdown-section:first-child:after{content:',';margin-right:2px;}.products .sale-product-daily-deal .porto_countdown-amount,.products .sale-product-daily-deal .porto_countdown-period{font-size:13px;font-weight:500;padding:0 1px;}.products .sale-product-daily-deal .porto_countdown-section:last-child .porto_countdown-period{padding:0;}.products .sale-product-daily-deal:after{content:'';display:table;clear:both;}@media (min-width:992px){#header .header-main .container .header-left{padding:20px 0;}}#header.sticky-header .header-main .container .header-left{padding:10px 0;}</style>  <noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
  <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://quorumventures.co.ke/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.7.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<script type="text/javascript">function setREVStartSize(e){                 
            try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
              if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})          
            }catch(d){console.log("Failure at Presize of Slider:"+d)}           
          };</script>
<style type="text/css" title="dynamic-css" class="options-output">{color:#527c16;}{color:#EFEFEF;}{color:#ffffff;}</style><style type="text/css" data-type="vc_custom-css">#our-button{
    background :#527C16;
}</style><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1534265455682{margin-right: 5px !important;background-color: #1e73be !important;border-radius: 5px !important;}.vc_custom_1534255394105{margin-left: 5px !important;background-color: #81d742 !important;border-radius: 5px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css"></head>
<body class="home page-template-default page page-id-143 full blog-1  woocommerce-no-js yith-wcan-free woocommerce-wishlist woocommerce woocommerce-page wpb-js-composer js-comp-ver-5.4.7 vc_responsive">
    
    <div class="page-wrapper"><!-- page wrapper -->

        
                    <div class="header-wrapper clearfix"><!-- header wrapper -->
                                
                    <header id="header" class="header-8 search-sm">
    
    <div class="header-main">
        <div class="container">
            <div class="header-left">
                <h1 class="logo">    <a href="http://quorumventures.co.ke/" title="Quorum Ventures - Building a firm business network" rel="home">
                <img class="img-responsive standard-logo" src="//quorumventures.co.ke/wp-content/uploads/2018/08/Quroum-logo.jpeg" alt="Quorum Ventures" /><img class="img-responsive retina-logo" src="//quorumventures.co.ke/wp-content/uploads/2018/08/Quroum-logo.jpeg" alt="Quorum Ventures" style="display:none;" />            </a>
    </h1>            </div>
            <div class="header-center">
                <div id="main-menu">
                    <ul id="menu-main-menu" class="main-menu mega-menu show-arrow effect-down subeffect-fadein-left"><li id="nav-menu-item-541" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-143 current_page_item active narrow "><a href="http://quorumventures.co.ke/" class=" current">Home</a></li>
<li id="nav-menu-item-597" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/about-us/" class="">About Us</a></li>
<li id="nav-menu-item-879" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/our-portfolio/" class="">Our Portfolio</a></li>
<li id="nav-menu-item-684" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/670-2/" class="">Blog</a></li>
<li id="nav-menu-item-891" class="menu-item menu-item-type-custom menu-item-object-custom  narrow "><a href="http://shop.quorumventures.co.ke/quotes/requestquote.php" class="">Enquire</a></li>
<li id="nav-menu-item-892" class="menu-item menu-item-type-custom menu-item-object-custom  narrow "><a href="http://shop.quorumventures.co.ke/quotes/makepayment.php" class="">Pay</a></li>
<li id="nav-menu-item-791" class="menu-item menu-item-type-post_type menu-item-object-page  narrow "><a href="http://quorumventures.co.ke/contact/" class="">Contact Us</a></li>
</ul>                </div>
            </div>
            <div class="header-right search-popup">
                                <div class="">
                                        <a class="mobile-toggle"><i class="fa fa-reorder"></i></a>
                    <div class="block-nowrap">
                            <div class="searchform-popup">
        <a class="search-toggle"><i class="fa fa-search"></i></a>
            <form action="http://quorumventures.co.ke/" method="get"
        class="searchform ">
        <fieldset>
            <span class="text"><input name="s" id="s" type="text" value="" placeholder="Search&hellip;" autocomplete="off" /></span>
                        <span class="button-wrap"><button class="btn btn-special" title="Search" type="submit"><i class="fa fa-search"></i></button></span>
        </fieldset>
    </form>
        </div>
                        </div>

                                    </div>
    
                
            </div>
        </div>
        
<div id="nav-panel" class="">
    <div class="container">
        <div class="mobile-nav-wrap">
            <div class="menu-wrap"><ul id="menu-main-menu-1" class="mobile-menu accordion-menu"><li id="accordion-menu-item-541" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-143 current_page_item active"><a href="http://quorumventures.co.ke/" rel="nofollow" class=" current ">Home</a></li>
<li id="accordion-menu-item-597" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/about-us/" rel="nofollow" class="">About Us</a></li>
<li id="accordion-menu-item-879" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/our-portfolio/" rel="nofollow" class="">Our Portfolio</a></li>
<li id="accordion-menu-item-684" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/670-2/" rel="nofollow" class="">Blog</a></li>
<li id="accordion-menu-item-891" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="http://shop.quorumventures.co.ke/quotes/requestquote.php" rel="nofollow" class="">Enquire</a></li>
<li id="accordion-menu-item-892" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="http://shop.quorumventures.co.ke/quotes/makepayment.php" rel="nofollow" class="">Pay</a></li>
<li id="accordion-menu-item-791" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://quorumventures.co.ke/contact/" rel="nofollow" class="">Contact Us</a></li>
</ul></div>        </div>
    </div>
</div>
    </div>
</header>
</div><!-- end header wrapper -->
        
   <div class="container" style="margin-top: 20px;">
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="icon-box">
          <i class="material-icons">&#xE876;</i>
        </div>        
        <h4 class="modal-title">Thank You.</h4> 
      </div>
      <div class="modal-body">
        <p class="text-center">Your payment has gone through.</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>   
    <div class="row">
      <?php
           if(isset($_GET['pesapal_merchant_reference'])&&isset($_GET['pesapal_transaction_tracking_id']))
           {
            $updatedb = 'http://localhost/quorum_ventures_shop/quoramapi/index.php/quoterequest/updatepaydetails';
            $post_datadb=array('tracker' => $_GET['pesapal_transaction_tracking_id'],'ref'=>$_GET['pesapal_merchant_reference']); 
            $curre=curl_init($updatedb);
            curl_setopt($curre, CURLOPT_POST, true);
            curl_setopt($curre, CURLOPT_POSTFIELDS, $post_datadb);
            curl_setopt($curre, CURLOPT_RETURNTRANSFER, true);
            $resdbup = curl_exec($curre);
            $resdbarr=json_decode($resdbup);
             if($resdbarr->code==1)
               {
                
            ?>
           <script type="text/javascript">
            $(function(){
               $("#myModal").modal('show');
            });
            
           </script>
            <?php
             }
           }
        ?>
      <div class="col-md-7">
        <div class="panel panel-default">
      <div class="panel-heading"><p>Fill in the required information.</p><br><p>Fields marked with (*) are mandatory</p></div>
      <div class="panel-body">
        
        <img src="../assets/img/loading.gif" id="loading-indicator" style="display:none">

         <form action="pesapal-iframe.php" method="post">
        <div class="form-group">
     <input required="required" type="text" name="amount"  class="form-control"  placeholder="Amount In Kenyan Shillings" />
     </div>
     <div class="form-group">
     <input type="hidden" name="type" value="MERCHANT" readonly="readonly" class="form-control" />
      </div>

      <div class="form-group">
     <input required="required" type="text" name="customer"  class="form-control"  placeholder="Your Name" /></td>
     </div>
     <div class="form-group">
     <input required="required" type="text" name="company"  class="form-control" placeholder="Your Company Name" /></td>
     </div>
       <div class="form-group">
     <input required="email" type="text" name="email"  class="form-control"  placeholder="Your email" /></td>
     </div>
     <div class="form-group">
     <input required="required" type="text" name="quote_number"  class="form-control" placeholder="Your Quote Number" />
   </div>
    <div class="form-group">
     <input required="required" type="text" name="qoute_title"  class="form-control" placeholder="Your Quote Title" /></td>
   </div>
   <div class="form-group">
    <textarea required="required" class="form-control" name="order_details" placeholder="Your Quote Details" ></textarea>
  </div>
    
    
  <div class="form-group">
   <input type="submit" value="Make Payment"  class="form-control btn btn-large  greenbackground" /></td>
   </div>
</form></div>
<div style="text-align: right; text-decoration: underline;"><a href="http://shop.quorumventures.co.ke/quotes/requestquote.php">Request Quote</a></div>
    </div>
        
      </div>
       <div class="col-md-5" >
        <div class="container" style="margin-top: 20px;">
          <h5 style="background-color:#333333; color: white; font-size: 15px;">Our Mission</h5>
            <p style=" margin: 0 auto; color:#333333;"  class="text-left">
Lorem ipsum dolor sit amet, consectetur adipiscing metus elit.
Quisque rutrum pellentesque imperdiet. Lorem ipsum dolor sit
amet, consectetur adipiscing metus elit. Quisque rutrum
pellentesque imperdiet</p>
<h5 style="background-color:#333333; color: white; font-size15px;">Our Vision</h5>
            <p style="  color: #333333;" class="text-left">
Lorem ipsum dolor sit amet, consectetur adipiscing metus elit.
Quisque rutrum pellentesque imperdiet. Lorem ipsum dolor sit
amet, consectetur adipiscing metus elit. Quisque rutrum
pellentesque imperdiet.</p>

        </div>
      </div>
    </div>
   </div>        
    
        
       

            
<div class="footer-wrapper ">

                
                    
<div id="footer" class="footer-2">
            <div class="footer-main">
            <div class="container">
                
                                    <div class="row">
                                                        <div class="col-lg-3">
                                    <aside id="nav_menu-2" class="widget widget_nav_menu"><h3 class="widget-title">Logistics</h3><div class="menu-footer-1-container"><ul id="menu-footer-1" class="menu"><li id="menu-item-794" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-794"><a href="http://quorumventures.co.ke/portfolio/roadtransportation/">Road Transportation</a></li>
<li id="menu-item-795" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-795"><a href="http://quorumventures.co.ke/portfolio/non-vessel-operating-common-carrier-nvocc-and-cargo-consolidation/">Non-Vessel Operating Common Carrier (NVOCC) and Cargo Consolidation</a></li>
<li id="menu-item-796" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-796"><a href="http://quorumventures.co.ke/portfolio/project-cargo/">Project Cargo</a></li>
</ul></div></aside>                                </div>
                                                            <div class="col-lg-3">
                                    <aside id="nav_menu-3" class="widget widget_nav_menu"><h3 class="widget-title">PROCUREMENT</h3><div class="menu-footer2-container"><ul id="menu-footer2" class="menu"><li id="menu-item-839" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-839"><a href="http://quorumventures.co.ke/portfolio/custom-manufactured-materials-equipment/">Custom Manufactured Materials &#038; Equipment</a></li>
<li id="menu-item-841" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-841"><a href="http://quorumventures.co.ke/portfolio/procurement/">Wide Range of Products</a></li>
<li id="menu-item-800" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-800"><a href="http://quorumventures.co.ke/portfolio/project-cargo/">Project Cargo</a></li>
</ul></div></aside>                                </div>
                                                            <div class="col-lg-4">
                                    <aside id="nav_menu-4" class="widget widget_nav_menu"><h3 class="widget-title">SUPPLIES</h3><div class="menu-footer3-container"><ul id="menu-footer3" class="menu"><li id="menu-item-864" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-864"><a href="http://quorumventures.co.ke/portfolio/commodity-trading-supplies/">Construction Tools</a></li>
<li id="menu-item-863" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-863"><a href="http://quorumventures.co.ke/portfolio/safety-equipment/">Safety Equipment</a></li>
<li id="menu-item-862" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-862"><a href="http://quorumventures.co.ke/portfolio/sleeping-mats-and-tents/">Sleeping Mats and Tents</a></li>
<li id="menu-item-861" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-861"><a href="http://quorumventures.co.ke/portfolio/containers-and-pre-fabricated-units/">Containers and Pre-Fabricated Units</a></li>
<li id="menu-item-860" class="menu-item menu-item-type-post_type menu-item-object-portfolio menu-item-860"><a href="http://quorumventures.co.ke/portfolio/solar-equipment/">Solar Equipment</a></li>
</ul></div></aside>                                </div>
                                                            <div class="col-lg-2">
                                    <aside id="contact-info-widget-2" class="widget contact-info"><h3 class="widget-title">Contact Us</h3>        <div class="contact-info contact-info-block">
            <p>Head office Nairobi - NAS Office Suites Kilimani.</p>
            <ul class="contact-details">
                                <li><i class="fa fa-phone"></i> <strong>Phone:</strong> <span> +254202131761</span></li>                <li><i class="fa fa-envelope"></i> <strong>Email:</strong> <span><a href="mailto:info@quorumventures.co.ke">info@quorumventures.co.ke</a></span></li>                            </ul>
                    </div>

        </aside>                                </div>
                                                </div>
                
                            </div>
        </div>
    
        <div class="footer-bottom">
        <div class="container">
                        <div class="footer-left">
                                 Copyright © 2018.Quorum Ventures Limited. All rights reserved. | Designed and Powered by  <a href="http://maliwatt.co.ke/"> Maliwatt Technologies </a>            </div>
            
            
                    </div>
    </div>
    </div>


                
            </div>

        
    </div><!-- end wrapper -->
    

<!--[if lt IE 9]>
<script src="http://quorumventures.co.ke/wp-content/themes/porto/js/html5shiv.min.js"></script>
<script src="http://quorumventures.co.ke/wp-content/themes/porto/js/respond.min.js"></script>
<![endif]-->

        <script type="text/javascript" id="modal">
          jQuery(window).on( 'load', function(){
            startclock();
          });
          function stopclock (){
            if(timerRunning) clearTimeout(timerID);
            timerRunning = false;
            //document.cookie="time=0";
          }
          function showtime () {
            var now = new Date();
            var my = now.getTime() ;
            now = new Date(my-diffms) ;
            //document.cookie="time="+now.toLocaleString();
            timerID = setTimeout('showtime()',10000);
            timerRunning = true;
          }
          function startclock () {
            stopclock();
            showtime();
          }
          var timerID = null;
          var timerRunning = false;
          var x = new Date() ;
          var now = x.getTime() ;
          var gmt = 1534353365 * 1000 ;
          var diffms = (now - gmt) ;
        </script>
                <script type="text/javascript" id="info-bar">
          jQuery(window).on( 'load', function(){
            startclock();
          });
          function stopclock (){
            if(timerRunning) clearTimeout(timerID);
            timerRunning = false;
            //document.cookie="time=0";
          }
          function showtime () {
            var now = new Date();
            var my = now.getTime() ;
            now = new Date(my-diffms) ;
            //document.cookie="time="+now.toLocaleString();
            timerID = setTimeout('showtime()',10000);
            timerRunning = true;
          }
          function startclock () {
            stopclock();
            showtime();
          }
          var timerID = null;
          var timerRunning = false;
          var x = new Date() ;
          var now = x.getTime() ;
          var gmt = 1534353365 * 1000 ;
          var diffms = (now - gmt) ;
        </script>
                <script type="text/javascript" id="slidein">
          jQuery(window).on( 'load', function(){
            startclock();
          });
          function stopclock (){
            if(timerRunning) clearTimeout(timerID);
            timerRunning = false;
            //document.cookie="time=0";
          }

          function showtime () {
            var now = new Date();
            var my = now.getTime() ;
            now = new Date(my-diffms) ;
            //document.cookie="time="+now.toLocaleString();
            timerID = setTimeout('showtime()',10000);
            timerRunning = true;
          }

          function startclock () {
            stopclock();
            showtime();
          }
          var timerID = null;
          var timerRunning = false;
          var x = new Date() ;
          var now = x.getTime() ;
          var gmt = 1534353365 * 1000 ;
          var diffms = (now - gmt) ;
        </script>
              <style>
      #splite_popup_box {
        background: #EFEFEF;
        border-bottom: 5px solid #527c16;
        border-radius: 20px;
      }
      #splite_popup_title,
      #splite_popup_box div.wpcf7-response-output,
      a.splite_sideEnquiry {
        background-color: #527c16;
        color: #ffffff;  
      }
      #splite_popup_description {  
        color: #959595;  
      }
      #splite_popupBoxClose {
         color: #ffffff;  
      }           
      #splite_popup_box  div.wpcf7 img.ajax-loader,
      #splite_popup_box div.wpcf7 span.ajax-loader.is-active {
        box-shadow: 0 0 5px 1px #527c16;
      } 
      a.splite_sideEnquiry {
        background: ;       
      }
      
              #splite_popup_box input.wpcf7-form-control.wpcf7-submit {
          background: #527c16;
          letter-spacing: 1px;
          padding: 10px 15px;  
          text-align: center;
          border: 0; 
          box-shadow: none;   
        }
            #splite_popup_title {
        color: #F1F1F1;
        font-family: Open Sans;
        font-size: 28px;
        font-weight: ;
        line-height: 32px;
      }
      #splite_popup_description {
        color: #484848;
        font-family: Noto Sans;
        font-size: 13px;
        font-weight: ;
        line-height: 21px;
        text-align: center;
      }
      a.splite_sideEnquiry {
        color: #ffffff;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        font-weight: 700;
        line-height: 18px;
      }
      #splite_popup_box .wpcf7-form-control.wpcf7-submit {        
        color: #F1F1F1;
        font-family: Open Sans;
        font-size: 22px;
        font-weight: 700;
        line-height: 24px;
      }
      #splite_popup_box span.wpcf7-not-valid-tip{
           
      }   </style>
    
    <!-- SP Pro - Popup Box Curtain Arrangement -->
    <div id="splite_curtain" onClick="splite_unloader();" style=""></div>
    <div class="splite_popup_animator" data-loadspeed="0.50" data-loadeffect="fadeIn" data-unloadeffect="fadeOut" data-unloadspeed="0.50"></div>
    <div id="splite_popup_box" class="layout_centered manage">        
      <div id="splite_popup_title">Get a Quote Today</div>      
      <div id="splite_form_container" class="">
        <p id="splite_popup_description">We are glad that you preferred to contact us. Please fill our short form and one of our friendly team members will contact you back.</p>
        <div role="form" class="wpcf7" id="wpcf7-f773-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/#wpcf7-f773-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="773" />
<input type="hidden" name="_wpcf7_version" value="5.0.3" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f773-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
</div>
<p><label> Your Name (required)<br />
    <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span> </label></p>
<p><label> Your Email (required)<br />
    <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span> </label></p>
<p><label> Your Phone No. (required)<br />
    <span class="wpcf7-form-control-wrap tel-590"><input type="tel" name="tel-590" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" /></span><br />
 </label></p>
<p><label> Subject<br />
    <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" /></span> </label></p>
<p><label> Your Message<br />
    <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </label></p>
<p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" /></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>     </div>
      <!--<div class="success" style="display: none;">Successfully Submitted ...</div>-->
      <a id="splite_popupBoxClose" onClick="splite_unloader();">X</a>  
    </div>
    
          <a onClick="splite_loader();" class="splite_sideEnquiry pos_right on_mobile enabled_on_mobile">GET A QUOTE</a>
        
    <!-- Slick Popup Lite Box and Curtain Arrangement -->   
      <script type="text/javascript">
    var c = document.body.className;
    c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
    document.body.className = c;
  </script>
        <script type="text/javascript">
        function revslider_showDoubleJqueryError(sliderID) {
          var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
          errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
          errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
          errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
          errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
            jQuery(sliderID).show().html(errorMessage);
        }
      </script>
      <link rel='stylesheet' property='stylesheet' id='vc_google_fonts_abril_fatfaceregular-css'  href='//fonts.googleapis.com/css?family=Abril+Fatface%3Aregular&#038;ver=4.9.4' type='text/css' media='all' />
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/quorumventures.co.ke\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.4.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_bfa46a7a2136c92a6c20aa47a7a69c97","fragment_name":"wc_fragments_bfa46a7a2136c92a6c20aa47a7a69c97"};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.4.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-ajax-search/assets/js/yith-autocomplete.min.js?ver=1.2.7'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.6'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js?ver=1.2.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var yith_wcwl_l10n = {"ajax_url":"\/wp-admin\/admin-ajax.php","redirect_to_cart":"no","multi_wishlist":"","hide_add_button":"1","is_user_logged_in":"","ajax_loader_url":"http:\/\/quorumventures.co.ke\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif","remove_from_wishlist_after_add_to_cart":"yes","labels":{"cookie_disabled":"We are sorry, but this feature is available only if cookies are enabled on your browser.","added_to_cart_message":"<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"},"actions":{"add_to_wishlist_action":"add_to_wishlist","remove_from_wishlist_action":"remove_from_wishlist","move_to_another_wishlist_action":"move_to_another_wishlsit","reload_wishlist_and_adding_elem_action":"reload_wishlist_and_adding_elem"}};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js?ver=2.2.3'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/comment-reply.min.js?ver=4.9.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/wp-util.min.js?ver=4.9.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_variation_params = {"wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_no_matching_variations_text":"Sorry, no products matched your selection. Please choose a different combination.","i18n_make_a_selection_text":"Please select some product options before adding this product to your cart.","i18n_unavailable_text":"Sorry, this product is unavailable. Please choose a different combination."};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js?ver=3.4.4'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=5.4.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var js_porto_vars = {"rtl":"","ajax_url":"http:\/\/quorumventures.co.ke\/wp-admin\/admin-ajax.php","change_logo":"0","container_width":"1170","grid_gutter_width":"30","show_sticky_header":"1","show_sticky_header_tablet":"1","show_sticky_header_mobile":"1","ajax_loader_url":"\/\/quorumventures.co.ke\/wp-content\/themes\/porto\/images\/ajax-loader@2x.gif","category_ajax":"1","prdctfltr_ajax":"","show_minicart":"0","slider_loop":"1","slider_autoplay":"1","slider_autoheight":"1","slider_speed":"5000","slider_nav":"","slider_nav_hover":"1","slider_margin":"","slider_dots":"1","slider_animatein":"","slider_animateout":"","product_thumbs_count":"4","product_zoom":"1","product_zoom_mobile":"1","product_image_popup":"1","zoom_type":"inner","zoom_scroll":"1","zoom_lens_size":"200","zoom_lens_shape":"square","zoom_contain_lens":"1","zoom_lens_border":"1","zoom_border_color":"#888888","zoom_border":"0","screen_lg":"1200","mfp_counter":"%curr% of %total%","mfp_img_error":"<a href=\"%url%\">The image<\/a> could not be loaded.","mfp_ajax_error":"<a href=\"%url%\">The content<\/a> could not be loaded.","popup_close":"Close","popup_prev":"Previous","popup_next":"Next","request_error":"The requested content cannot be loaded.<br\/>Please try again later."};
/* ]]> */
</script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-content/themes/porto/js/theme.min.js?ver=4.3.1'></script>
<script type='text/javascript' src='http://quorumventures.co.ke/wp-includes/js/wp-embed.min.js?ver=4.9.4'></script>

    <script type="text/javascript">
        jQuery(document).ready(function(){});    </script>
</body>
</html>