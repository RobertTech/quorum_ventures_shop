<?php
include('../header.php');
 ?>       
   <div class="container" style="margin-top: 20px;">
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="icon-box">
          <i class="material-icons">&#xE876;</i>
        </div>        
        <h4 class="modal-title">Thank You.</h4> 
      </div>
      <div class="modal-body">
        <p class="text-center">Your payment has gone through.</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>   
    <div class="row">
      <?php
           if(isset($_GET['pesapal_merchant_reference'])&&isset($_GET['pesapal_transaction_tracking_id']))
           {
            //replace this when deploying
            $updatedb = 'http://lhpa661sep.quorumventures.co.ke/index.php/quoterequest/updatepaydetails';
            $post_datadb=array('tracker' => $_GET['pesapal_transaction_tracking_id'],'ref'=>$_GET['pesapal_merchant_reference']); 
            $curre=curl_init($updatedb);
            curl_setopt($curre, CURLOPT_POST, true);
            curl_setopt($curre, CURLOPT_POSTFIELDS, $post_datadb);
            curl_setopt($curre, CURLOPT_RETURNTRANSFER, true);
            $resdbup = curl_exec($curre);
            $resdbarr=json_decode($resdbup);
             if($resdbarr->code==1)
               {
                
            ?>
           <script type="text/javascript">
            $(function(){
               $("#myModal").modal('show');
            });
            
           </script>
            <?php
             }
           }
        ?>
      <div class="col-md-7">
        <div class="panel panel-default">
      <div class="panel-heading"><p>Fill in the required information.</p><br><p>Fields marked with (*) are mandatory</p></div>
      <div class="panel-body">
        
        <img src="../assets/img/loading.gif" id="loading-indicator" style="display:none">

         <form action="pesapal-iframe.php" method="post">
        <div class="form-group">
     <input required="required" type="text" name="amount"  class="form-control"  placeholder="Amount In Kenyan Shillings" />
     </div>
     <div class="form-group">
     <input type="hidden" name="type" value="MERCHANT" readonly="readonly" class="form-control" />
      </div>

      <div class="form-group">
     <input required="required" type="text" name="customer"  class="form-control"  placeholder="Your Name" /></td>
     </div>
     <div class="form-group">
     <input required="required" type="text" name="company"  class="form-control" placeholder="Your Company Name" /></td>
     </div>
       <div class="form-group">
     <input required="email" type="text" name="email"  class="form-control"  placeholder="Your email" /></td>
     </div>
     <div class="form-group">
     <input required="required" type="text" name="quote_number"  class="form-control" placeholder="Your Quote Number" />
   </div>
    <div class="form-group">
     <input required="required" type="text" name="qoute_title"  class="form-control" placeholder="Your Quote Title" /></td>
   </div>
   <div class="form-group">
    <textarea required="required" class="form-control" name="order_details" placeholder="Your Quote Details" ></textarea>
  </div>
    
    
  <div class="form-group">
   <input type="submit" value="Make Payment"  class="form-control btn btn-large btn-success  greenbackground " style="text-align: center;" /></td>
   </div>
</form></div>
<div style="text-align: right; text-decoration: underline;"><a href="http://shop.quorumventures.co.ke/quotes/requestquote.php">Request Quote</a></div>
    </div>
        
      </div>
       <div class="col-md-5" >
        <div class="container" style="margin-top: 20px;">
          <h5 style="background-color:#333333; color: white; font-size: 15px;">Our Mission</h5>
            <p style=" margin: 0 auto; color:#333333;"  class="text-left">
Lorem ipsum dolor sit amet, consectetur adipiscing metus elit.
Quisque rutrum pellentesque imperdiet. Lorem ipsum dolor sit
amet, consectetur adipiscing metus elit. Quisque rutrum
pellentesque imperdiet</p>
<h5 style="background-color:#333333; color: white; font-size15px;">Our Vision</h5>
            <p style="  color: #333333;" class="text-left">
Lorem ipsum dolor sit amet, consectetur adipiscing metus elit.
Quisque rutrum pellentesque imperdiet. Lorem ipsum dolor sit
amet, consectetur adipiscing metus elit. Quisque rutrum
pellentesque imperdiet.</p>

        </div>
      </div>
    </div>
   </div>        
    
        
       

            
<?php
include('../footer.php');
?>
