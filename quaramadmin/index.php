<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Quarum Ventures</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>

        <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

        <script type="text/javascript" src="assets/js/core/app.js"></script>

        <script type="text/javascript" src="assets/js/plugins/notifications/pnotify.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/components_notifications_pnotify.js"></script>
        <script type="text/javascript" src="assets/js/load_image.js"></script>
        <script type="text/javascript" src="assets/js/jsurl.js"></script>

        <script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/uploader_bootstrap.js"></script>

        <script type="text/javascript" src="assets/js/plugins/notifications/bootbox.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/notifications/sweet_alert.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/components_modals.js"></script>

        <script type="text/javascript" src="assets/js/core/libraries/jquery_ui/interactions.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/form_select2.js"></script>
        <script type="text/javascript" src="assets/js/pages/invoice_archive.js"></script>


<!--<script type="text/javascript" src="assets/js/pages/components_notifications_pnotify.js"></script>-->
        <script>

            $(document).ready(function () {
                $("#status-login").hide();
            });
            $(function () {
                $('#login-form').submit(function (e) {
                    e.preventDefault();
                        var url =base_url2 + "Quoterequest/login";
                console.log(url)
                      $.ajax({
                        url: url,
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                          var parse = JSON.parse(data);
                          console.log(parse);
                          if(parse['code']==1)
                          {
                            lstorage(parse);
                              new PNotify({
                                    text: 'Welcome',
                                    addclass: 'bg-success'
                                });
                              window.location="admin/payments.php"
                          }   
                          else
                          {
                              new PNotify({
                                    text: 'Wrong password/email combination',
                                    addclass: 'bg-warning'
                                });
                          }                   
                            },
                        error: function () {}
                    });
                  
                  
                });

                $('#pnotify-solid-primary2').on('click', function () {
                    new PNotify({
                        title: 'Primary notice',
                        text: 'Check me out! I\'m a notice.',
                        addclass: 'bg-primary'
                    });
                });

            });

            function lstorage(data) {
                for (var key in data) {
                    console.log(key);
                    localStorage.setItem(key, data[key]);

                }
//      alert("key is code and value is "+localStorage.getItem('code'));
            }
        </script>

    </head>

    <body class="login-container">

        <!-- Main navbar -->
        <div class="navbar navbar-inverse" style="background-color:#527C16;
      color:white; ">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"></a>

                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                </ul>
            </div>


        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content">

                        <!-- Simple login form -->
                        <form id="login-form" method="post">
                            <div class="panel panel-body col-lg-6 col-lg-offset-3">
                                <div class="text-center">
                                    <div class="icon-object border-slate-300 "><img style="width:70px; height: 50px;" src="assets/images/1.jpeg"/></div>
                                    <h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
                                </div>
                                <div class="row">
                                   
                                    <div class="form-group col-lg-12">
                                        <input type="email" required class="form-control" id="phone" name="email" id="email">

                                    </div>

                                </div>





                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" required class="form-control" id="password" name="password" placeholder="Password">
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <img id="status-login" class="text-center center-block "  src="assets/loader/loader.gif"/>
                                    <input type="submit" value="Sign In" class="btn text-center center-block" style="background-color:#527C16;
      color:white; ">
                                </div>
                                <div class="text-center">
                                    <!-- <a href="reset.php">Reset Password</a> -->
                                </div>
                            </div>
                        </form>
                        <!-- /simple login form -->

                        <!-- Footer -->
                        
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>


</html>
