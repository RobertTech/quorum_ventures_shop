<!-- Main navbar -->
<div class="navbar navbar-inverse greenbackground">
    <div class="navbar-header">
        <a class="navbar-brand" href="#"><img style="width:70px; height: 50px;" src="../assets/images/1.jpg"/></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>


        </ul>

        <p class="navbar-text">
            <span class="label bg-success">Online</span>
        </p>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown dropdown-user">
                    <a id="placehere2" class="dropdown-toggle" data-toggle="dropdown">
                        <style>
                            #placehere2 img{
                                width: 30px;
                                height: 30px;
                            }
                        </style>
                        <div >
                            
                        </div>
                        <!--<img src="../assets/images/placeholder.jpg" alt="">-->
                        <span><p id="nameUsers"></p></span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="../cc/profile.php"><i class="icon-user-plus"></i> My profile</a></li>
                        <li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <script>
            document.getElementById("nameUsers").innerHTML = localStorage.getItem('first_name') + ' ' + localStorage.getItem('last_name');
            
        </script>
        <script type="text/javascript">
            if (localStorage.getItem('image_url')) {
                var elem = document.createElement("img");
                elem.setAttribute("src", localStorage.getItem('image_url'));
                elem.setAttribute("height", "768");
                elem.setAttribute("width", "1024");
                elem.setAttribute("alt", "Flower");
                document.getElementById("placehere2").appendChild(elem);
            } else {
                var elem = document.createElement("img");
                elem.setAttribute("src", "../assets/images/placeholder.jpg");
                elem.setAttribute("height", "10");
                elem.setAttribute("width", "10");
                elem.setAttribute("alt", "Flower");
                document.getElementById("placehere2").appendChild(elem);
            }

        </script>
    </div>
</div>
<!-- /main navbar -->
