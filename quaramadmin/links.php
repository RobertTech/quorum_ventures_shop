<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="../assets/css/core.css" rel="stylesheet" type="text/css">
<link href="../assets/css/components.css" rel="stylesheet" type="text/css">
<link href="../assets/css/colors.css" rel="stylesheet" type="text/css">

<!-- /global stylesheets -->

<!-- Core JS files -->
<script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>

<script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>

<script type="text/javascript" src="../assets/js/core/app.js"></script>

<script type="text/javascript" src="../assets/js/plugins/notifications/pnotify.min.js"></script>

<script type="text/javascript" src="../assets/js/pages/components_notifications_pnotify.js"></script>
<script type="text/javascript" src="../assets/js/load_image.js"></script>
<script type="text/javascript" src="../assets/js/jsurl.js"></script>

<script type="text/javascript" src="../assets/js/plugins/uploaders/fileinput.min.js"></script>

<script type="text/javascript" src="../assets/js/pages/uploader_bootstrap.js"></script>

<script type="text/javascript" src="../assets/js/plugins/notifications/bootbox.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/anytime.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/pickadate/picker.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/pickadate/picker.date.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/pickadate/picker.time.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/pickadate/legacy.js"></script>


<script type="text/javascript" src="../assets/js/pages/picker_date.js"></script>

<script type="text/javascript" src="../assets/js/pages/components_modals.js"></script>

<script type="text/javascript" src="../assets/js/core/libraries/jquery_ui/interactions.min.js"></script>

<script type="text/javascript" src="../assets/js/pages/form_select2.js"></script>
<script type="text/javascript" src="../assets/js/pages/invoice_archive.js"></script>

