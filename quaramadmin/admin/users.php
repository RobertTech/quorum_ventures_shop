<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Quarum Ventures | Admin</title>

        <?php include("../links.php") ?>
        <!-- /theme JS files -->
               <script>
            $(document).ready(function () {
  $(".status-progress").hide();
                fetchusers = function()
                {
                   var formData = {
                'access_token': localStorage.getItem('access_token')
            };
           var url =base_url2 + "Quoterequest/fetchusers";
            $.post(url, formData,
                function (data) {

                    var datas = JSON.parse(data);
                    console.log(datas);
                    // var model = $('#selectsku');
                    // model.empty();
                    if(datas['code']==1)
                    {
                         var user_row="";   
                            
                           var no =0;  
                      $.each(datas['users'], function (k, v) {

                         no=no+1;
                            var status="<button class='btn btn-warning'>Not Paid</button>";
                            if(v.status==1)
                            {
                              status="<button class='btn btn-success'>Paid</button>";
                            }
                        user_row+="<tr><td>"+no+"</td><td>"+v.name+"</td><td>"+v.email+"</td><td>"+v.timestamp+"</td></tr>";
                          
                        });  
                       document.getElementById('payments').innerHTML="";
                        $("#payments").append(user_row);
                            $("#example").DataTable();
                    }
                    else
                    {
                        window.location ='../index.php'
                    }

                        
                });


                   
              
                }
              fetchusers();
                  $('#adduser_form').submit(function (e) {
                e.preventDefault();
                alert("Submit");
                 var url =base_url2 + "Quoterequest/adduser";
               console.log(url);
                 $.ajax({
                        url: url,
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                          var parse = JSON.parse(data);
                          if(parse['code']==1)
                          {
                             $("#myModal").modal('hide');
                             $("#myModal1").modal('show');
                             $("#adduser_form").trigger('reset');
                             fetchusers();
                          }                        },
                        error: function () {}
                    });
              });

                });
         

        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                       
                        <!-- /user menu -->

                        <?php include("../sidebar.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Users</span></h4>
                            </div>

                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                                <li class="active">Users</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">Users</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                                  <button type="button" class="btn btn-info btn-lg pull-right greenbackground" data-toggle="modal" data-target="#myModal">Register User</button>
                            </div>

                            <div class="panel-body">
                                <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Stock</a>-->
                              <!--   <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/> -->
                            </div>

                            <table class="table table-bordered" id="example">
                                <thead>
                                    <tr>
                                         <th>#</th>
                                            <th>Usernamel</th>
                                            <th>Email</th>
                                            <th>Date Registered</th>
                                           
                
                                    </tr>
                                </thead>
                                <tbody id="payments">
                                    
                                </tbody>

                            </table>
                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                       <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Register User</h4>
      </div>
      <div class="modal-body">
       <form action="#" method="post" name="adduser_form" id="adduser_form">
         <div class="form-group">
     <input required="required" type="text" name="name"  class="form-control"  placeholder="Username" />
     </div>
        <div class="form-group">
     <input required="required" type="email" name="email"  class="form-control"  placeholder="info@admin.com" />
     </div>
     <div class="form-group">
     <input type="password" name="type" value="" class="form-control" />
      </div>
      <div class="form-group">
     <input type="submit" name="submit" value="Register"  class="form-control btn btn-large greenbackground" style="text-align: center;" />
      </div>
       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
                        <!-- /vertical form modal -->



                        <!-- Vertical form modal -->
                        <div id="myModal1" class="modal fade">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="icon-box">
          <i class="material-icons">&#xE876;</i>
        </div>        
        <h4 class="modal-title">User Registered</h4> 
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>   
                        <!-- /vertical form modal -->




                        <!-- Vertical form modal -->
                       
                        <!-- /vertical form modal -->


                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
