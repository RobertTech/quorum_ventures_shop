<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka Pepe</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>

        <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

        <script type="text/javascript" src="assets/js/core/app.js"></script>

        <script type="text/javascript" src="assets/js/plugins/notifications/pnotify.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/components_notifications_pnotify.js"></script>
        <script type="text/javascript" src="assets/js/load_image.js"></script>
        <script type="text/javascript" src="assets/js/configs.js"></script>

        <script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/uploader_bootstrap.js"></script>

        <script type="text/javascript" src="assets/js/plugins/notifications/bootbox.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/notifications/sweet_alert.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/components_modals.js"></script>

        <script type="text/javascript" src="assets/js/core/libraries/jquery_ui/interactions.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/form_select2.js"></script>
        <script type="text/javascript" src="assets/js/pages/invoice_archive.js"></script>


<!--<script type="text/javascript" src="assets/js/pages/components_notifications_pnotify.js"></script>-->
        <script>

            $(document).ready(function () {
                $("#status-login").hide();
                $("#password2").keyup(validate);
            });
            $(function () {
                $('form').submit(function () {
                    $("#status-login").show();
                    var returned = false; // stops function from submitting form
                    var url = base_url + "user/change_password";

                    var s = $("#phone").val();

                    while (s.charAt(0) === '0')
                    {
                        s = s.substr(1);
                    }
                    var country = $("#selectcountry").val();
                    var loginPhone = country + s;


//                    var formData = {
//                        'phone': loginPhone,
//                        'password':$("#password").val()
//                    };

                    
                    var formData = $('form').serializeArray(); // serializing the json data 
                    formData.push({ name: "phone", value: loginPhone });
                   
                    console.log(formData);

                    var password_strength2 = document.getElementById("password_strength");
                if(password_strength2.innerHTML == "Weak"){
                    alert("Please make sure your password strength is not weak");
                    $(".status-progress").hide();
                }else{ 
                     var password3 = $("#password").val();
                      var password4 = $("#password2").val();
                    if(password3 == password4) {
                       
                       $.post(url, formData).done(function (data) {
                        returned = true;
                            new PNotify({
                                    text: 'Password reset successfully',
                                    addclass: 'bg-success'
                                });
                            window.location = "index.php";
                            $("#status-login").hide();
                        // var data = JSON.parse(data);
                        // lstorage(data); //call localstorage to store login credentials
                        // if (data['code'] == 1) {
                            
                        // } else {
                        //     new PNotify({
                        //         text: data['message'],
                        //         addclass: 'bg-warning'
                        //     });
                        //     $("#status-login").hide();

                        // }

                    }); 
                           
                    }
                    else {
                        $("#validate-status").text("Passwords do not match"); 
                        alert("Please make sure your passwords match");
                        $("#status-login").hide(); 
                    }

                    
                }
return returned; 
                    
                   
                });

                $('#pnotify-solid-primary2').on('click', function () {
                    new PNotify({
                        title: 'Primary notice',
                        text: 'Check me out! I\'m a notice.',
                        addclass: 'bg-primary'
                    });
                });

            });

            function lstorage(data) {
                for (var key in data) {
                    console.log(key);
                    localStorage.setItem(key, data[key]);

                }
//      alert("key is code and value is "+localStorage.getItem('code'));
            }

            function validate() {
              var password = $("#password").val();
              var password2 = $("#password2").val();

                
             
                if(password == password2) {
                   $("#validate-status").text("valid");        
                }
                else {
                    $("#validate-status").text("Passwords do not match");  
                }
                
            }
        </script>

    </head>

    <body class="login-container">

        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"></a>

                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                </ul>
            </div>


        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content">

                        <!-- Simple login form -->
                        <form id="login-form" method="post">
                            <div class="panel panel-body col-lg-6 col-lg-offset-3">
                                <div class="text-center">
                                    <div class="icon-object border-slate-300 "><img style="width:70px; height: 50px;" src="assets/images/1.jpg"/></div>
                                    <h5 class="content-group">Reset your account <small class="display-block">Enter your credentials below</small></h5>
                                </div>
                                <div class="row">
                                    <div class="form-group has-feedback has-feedback-left col-lg-6">
                                        <select required id="selectcountry" name="category_id" class="select">
                                            <optgroup label="Select country code">
                                                <option value="+254">Kenya</option>
                                            </optgroup>

                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="number" required class="form-control" id="phone" name="phonehidden" placeholder="07.....">

                                    </div>

                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" required class="form-control" id="password" name="password" placeholder="Password" onkeyup="CheckPasswordStrength(this.value)">
                                    <script type="text/javascript">
                                                            function CheckPasswordStrength(password) {
                                                                var password_strength = document.getElementById("password_strength");

                                                                //TextBox left blank.
                                                                if (password.length == 0) {
                                                                    password_strength.innerHTML = "";
                                                                    return;
                                                                }

                                                                //Regular Expressions.
                                                                var regex = new Array();
                                                                regex.push("[A-Z]"); //Uppercase Alphabet.
                                                                regex.push("[a-z]"); //Lowercase Alphabet.
                                                                regex.push("[0-9]"); //Digit.
                                                                regex.push("[$@$!%*#?&]"); //Special Character.

                                                                var passed = 0;

                                                                //Validate for each Regular Expression.
                                                                for (var i = 0; i < regex.length; i++) {
                                                                    if (new RegExp(regex[i]).test(password)) {
                                                                        passed++;
                                                                    }
                                                                }

                                                                //Validate for length of Password.
                                                                if (passed > 2 && password.length > 8) {
                                                                    passed++;
                                                                }

                                                                //Display status.
                                                                var color = "";
                                                                var strength = "";
                                                                switch (passed) {
                                                                    case 0:
                                                                    case 1:
                                                                        strength = "Weak";
                                                                        color = "red";
                                                                        break;
                                                                    case 2:
                                                                        strength = "Good";
                                                                        color = "darkorange";
                                                                        break;
                                                                    case 3:
                                                                    case 4:
                                                                        strength = "Strong";
                                                                        color = "green";
                                                                        break;
                                                                    case 5:
                                                                        strength = "Very Strong";
                                                                        color = "darkgreen";
                                                                        break;
                                                                }
                                                                password_strength.innerHTML = strength;
                                                                password_strength.style.color = color;
                                                            }
                                                        </script>
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                    <span id="password_strength"></span>
                                </div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" required class="form-control" id="password2" name="confirm_password" placeholder="Confirm Password">
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                </div>
                                <p id="validate-status"></p>


                                <div class="form-group">
                                    <img id="status-login" class="text-center center-block "  src="assets/loader/loader.gif"/>
                                    <input type="submit" value="Reset Password" class="btn text-center center-block" style="background:#FF3A1D;">
                                </div>
                                <div class="text-center">
                                    <a href="index.php">back to login</a>
                                </div>
                            </div>
                        </form>
                        <!-- /simple login form -->

                        <!-- Footer -->
                        <div class="footer text-muted text-center">
                            &copy; <?php date('Y'); ?>. <a href="#">Duka pepe</a>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>


</html>
