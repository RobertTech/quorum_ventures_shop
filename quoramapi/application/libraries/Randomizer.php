<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Randomizer
 *
 * @author mwamb
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Randomizer {
    //put your code herepublic $CI;
    public function __construct() { 
    $this->CI=& get_instance();
    }
    public function getSupplierCode(){
             $length=4;
     $randstr="";
     //our array add all letters and numbers if you wish
     $chars = array(
         'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 
         'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

     for ($rand = 0; $rand <= $length; $rand++) {
         $random = rand(0, count($chars) - 1);
         $randstr .= $chars[$random];
     }
     return $randstr;
    }
}
