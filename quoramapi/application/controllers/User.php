<?php
header('Access-Control-Allow-Origin: *');

class User extends CI_Controller {

	public function __construct(){
         parent::__construct();

         $this->load->model('User_Model'); //Load the Model here   

 }
	public function register()
	{
		$data['name']=$this->input->post('name');
		$data['email']=$this->input->post('email');
		$data['password']=password_hash($this->input->post('password'),PASSWORD_DEFAULT);
		$data['phone']=$this->input->post('phone');
		$gender['gender']=$this->input->post('gender');
		$response = $this->User_Model->register($data);
		echo $response;
	}
	public function login()
	{
		$data['phone']=$this->input->post('phone');
		$data['password']=$this->input->post('password');
		$response = $this->User_Model->login($data);
		echo $response;
	}
}
