<?php
header('Access-Control-Allow-Origin: *');
class Quoterequest extends CI_Controller {
 public function __construct() 
      { 
         parent::__construct(); 
         $this->load->helper(array('form', 'url'));
         $this->load->library('image');
         $this->load->model('Request_Model');
         $this->load->model('Management_Model');
      }
      public function adduser()
      {
           $data['email'] = $_POST['email'];
           $data['password'] = password_hash($this->input->post('password'),PASSWORD_DEFAULT);
           $data['name'] = $_POST['name'];
           $data['user_level'] =1;
           $response = $this->Request_Model->adduser($data);
           echo $response;
      }
        
      public function savepaydetails()
       {
       	
           $data['amount'] = $_POST['amount'];
           $data['order_details'] = $_POST['order_details'];
           $data['quote_number'] = $_POST['order_code'];
           $data['customer'] = $_POST['customername'];
           $data['company'] = $_POST['company'];
           $data['email'] = $_POST['email'];
           $response = $this->Request_Model->savepaydetails($data);
           echo $response;
       }
        public function fetch()
       {
       	 $access=$this->Management_Model->validate_request($this->input->post('access_token'));
         if($access['code']==1)
         {
             $response = $this->Request_Model->showpayments();
             echo $response;
         }
         else
         {
           $response['code']=0;
           $response['message']="Access Denied";
           echo json_encode($response);
           
         }
        //echo $this->input->post('access_token');//$this->input->post('access_token');
          
       }
        public function fetchusers()
       {
           $access=$this->Management_Model->validate_request($this->input->post('access_token'));
         if($access['code']==1)
         {
           $response = $this->Request_Model->showusers();
           echo $response;
            }
         else
         {
           $response['code']=0;
           $response['message']="Access Denied";
           echo json_encode($response);
           
         }
       }
       public function updatepaydetails()
       {
       	   $data['id'] = $_POST['ref'];
           $data['track_id'] = $_POST['tracker'];
           $data['status'] =1;
           $response = $this->Request_Model->updatepaydetails($data);
           echo $response;
       }
       public function login()
        {
          $data['email']=$this->input->post('email');
          $data['password']=$this->input->post('password');
          $response = $this->Request_Model->login($data);
          echo $response;
        }
        public function logout()
        {
          $data['access_token']=$this->input->post('access_token');
          $response = $this->Request_Model->logout($data);
          echo $response;
        }
      public function requestq()
      {
      	$data['company_name']=$this->input->post('client_company');
      	$data['name']=$this->input->post('client_name');
      	$data['email']=$this->input->post('client_email');
      	$data['q_topic']=$this->input->post('client_topic');
      	$data['q_details']=$this->input->post('client_details');

      	$code = $this->Request_Model->add($data);
      	
      	if($code==1)
      	{
                    $ci = get_instance();
					$ci->load->library('email');
					$config['protocol'] = "smtp";
					$config['smtp_host'] = "ssl://smtp.zoho.com";
					$config['smtp_port'] = "465";
					$config['smtp_user'] = "info@quorumventures.co.ke"; 
					$config['smtp_pass'] = "info2016";
					$config['charset'] = "utf-8";
					$config['mailtype'] = "html";
					$config['newline'] = "\r\n";

					$ci->email->initialize($config);
					$body=$this->load->view('quoteemail',['userName'=>$data['name'],'details'=>$this->input->post('client_details'),'body'=>$this->input->post('client_details'),'companyname'=>$this->input->post('client_company')],true);
					$ci->email->from('info@quorumventures.co.ke', 'Quorum Online');
					
					$list = array($data['email'],'info@quorumventure.co.ke');
					$ci->email->to($list);
					$this->email->reply_to($data['email'], $data['name']);
					$ci->email->subject('Quote Request: '.$this->input->post('client_topic'));
					$ci->email->message($body);
					
					if($ci->email->send())  
					{
						$response['code']=1;
						$reponse['message']='Quote Sumitted And email sent';
					}     
					else
					{
					    $response['code']=0;
						$reponse['message']='Quote Sumitted And not email sent';	
					}
          
      	}
      	else
      	{
               $response['code']=0;
						$reponse['message']='Quote not Sumitted .';
      	}
       echo json_encode($response);
    }
    
}
?>
