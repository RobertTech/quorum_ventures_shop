<?php
class User_Model extends CI_Model{
	public $users_table='users';
    public $activation_table='account_validation';
    public $login_details="login_details";
    public function __construct() {
        parent::__construct();
        
       $db=$this->load->database();
    }
	public function register($data)
	{
		$user_exist = $this->checkNewUser($data['phone']);
		$response['title']='Registration Status';
		if($user_exist==0)
		{
		 $this->db->insert('users',$data);
         $response['code']=1;
		 $response['message']="REegistration Succsessful. Please Activate Your Account";
		}
		else
		{
		$response['code']=0;
		$response['message']="Failed. Similar Credentials";	
		}
        
		return json_encode($response);
	}
	public function checkNewUser($phone){
        $this->db->select('id');
        $this->db->or_where('phone',$phone);
        $this->db->from('users');
        $count=$this->db->count_all_results();
        
        return $count;   
    }
    public function login($data)
    {
        $this->db->select('id');
        $this->db->or_where('phone',$data['phone']);
        $this->db->from('users');
        $phonefound=$this->db->count_all_results(); 
        if($phonefound==0)
		{
		   $response['code']=0;
		   $response['message']="No such User";
		
		}
		{
			$this->db->select('status');
            $this->db->or_where('phone',$data['phone']);
            $this->db->from('activations');
            $phonefound=$this->db->get(); 
            $returnedarray = $phonefound->result_array();
            if(sizeof($returnedarray)>0)
            {
                            if($returnedarray[0]['status']==1)
                            {
                                       $this->db->select('password,phone,id');
                                       $this->db->or_where('phone',$data['phone']);
                                       $this->db->from('users');
                                       $returneddata=$this->db->get(); 
                                       $array=$returneddata->result_array();
                                       if($array[0]['phone']==$data['phone'] && password_verify($data['password'],$array[0]['password']))
                                       {
                                        $login_data['user_id']=$array[0]['password'];
                                        $login_data['status']=1;
                                        $login_data['access_token']=password_hash($data['phone'],PASSWORD_DEFAULT);
                                        $this->db->insert('login_status',$login_data);
                                        $response['code']=1;
                                        $response['message']="Log Success";
                                        $response['access_token']=$login_data['access_token'];
                                        $response['userinfor']=$this->get_userinformation($data['phone']);
                                       }
                                       else
                                       {
                                       
                                        $response['code']=0;
                                        $response['message']="Log in Failed. Make Sure You using the right credentials";
                                       }

                            }
                            else
                            {
                                        $response['code']=0;
                                        $response['message']="You have Not Activated Your Account";
                            }  
            }
            else
            {
                            $response['code']=0;
                            $response['message']="You have Not Activated Your Account";
            } 
            

		}
        return json_encode($response);
        
    }
    public function get_userinformation($phone)
    {
      $this->db->select('name,email,phone,id');
      $this->db->or_where('phone',$phone);
      $this->db->from('users');  
      $returneddata=$this->db->get();
      $array=$returneddata->result_array();
      return $array;
    }
}
?>