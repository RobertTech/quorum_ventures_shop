<?php
class Request_Model extends CI_Model{
	// public $users_table='users';
 //    public $activation_table='account_validation';
    // public $login_details="login_details";
    public function __construct() {
        parent::__construct();
        
       $db=$this->load->database();
    }
     public function adduser($data)
      {
           $user_exist = $this->checkNewUser($data['email']);
            $response['title']='Registration Status';
            if($user_exist==0)
            {
                 $this->db->insert('users',$data);
                 $response['code']=1;
                 $response['message']="Registration Succsessful.";
            }
            else
            {
                 $response['code']=0;
                 $response['message']="Failed. Similar Credentials"; 
            }
                
            return json_encode($response);
      }
      public function checkNewUser($email)
        {
        $this->db->select('id');
        $this->db->or_where('email',$email);
        $this->db->from('users');
        $count=$this->db->count_all_results();
        
        return $count;   
       }
       public function logout($data)
    {
                                        $login_data['is_logged']=0;
                                        $login_data['access_token']="";
                                        $this->db->where('access_token',$data['access_token']);
                                        
                                        if($this->db->update('users',$login_data))
                                        {
                                         $response['code']=1;
                                        $response['message']="Logged out Successfully";
                                        return json_encode($response); 
                                        }
                                        else
                                        {
                                          $response['code']=0;
                                        $response['message']="Not succssful";
                                        return json_encode($response); 
                                        }
                                        
    }   
       public function login($data)
    {
        $this->db->select('id');
        $this->db->or_where('email',$data['email']);
        $this->db->from('users');
        $phonefound=$this->db->count_all_results(); 
        if($phonefound==0)
             {
                   $response['code']=0;
                   $response['message']="No such User";
             }
             else if($phonefound==1)
             {
                              
                
                                       $this->db->select('password,email');
                                       $this->db->or_where('email',$data['email']);
                                       $this->db->from('users');
                                       $returneddata=$this->db->get(); 
                                       $array=$returneddata->result_array();
                                       if($array[0]['email']==$data['email'] && password_verify($data['password'],$array[0]['password']))
                                       {
                                        
                                        $login_data['is_logged']=1;
                                        $login_data['access_token']=password_hash($data['email'],PASSWORD_DEFAULT);
                                        $this->db->where('email',$data['email']);
                                        $this->db->update('users',$login_data);
                                        $response['code']=1;
                                        $response['message']="Log Success";
                                        $response['access_token']=$login_data['access_token'];
                                        $response['userinfor']=$this->get_userinformation($data['email']);
                                       }
                                       else
                                       {
                                       
                                        $response['code']=0;
                                        $response['message']="Log in Failed. Make Sure You using the right credentials";
                                       }
             }
                   
 
           
            

      return json_encode($response);
        
    }
	public function add($data)
	{
    if($this->db->insert('quotes',$data))
    {   
        return 1;
    }
      else
      {
        return 0;
      }

	}
  public function savepaydetails($data)
  {
      if($this->db->insert('tracks',$data))
      {
        $this->db->select('*');
        $this->db->from('tracks');
        $this->db->order_by('timestamp ASC');
        $this->db->limit(1);
        $query=$this->db->get();
        $returneddata=$query->result_array();
        $response['code']=1;
        // $response['hello']='ddd';
        $response['id']=$returneddata[0]['id'];
        return json_encode($response);
      }
      else
      {
          $response['code']=0;
          $response['id']='none';
      }  
  }
   public function showpayments()
       {
           $this->db->select('*');
           $this->db->from('tracks');
           $this->db->order_by('timestamp ASC');
           $query=$this->db->get();
           $response['payments']=$query->result_array();
           $response['code']=1;
           return json_encode($response);
        
        }
         public function showusers()
       {
           $this->db->select('*');
           $this->db->from('users');
           $this->db->order_by('timestamp ASC');
           $query=$this->db->get();
           $response['users']=$query->result_array();
            $response['code']=1;
           return json_encode($response);
        
        }
  public function updatepaydetails($data)
  {
      $this->db->where('id', $data['id']);
      if($this->db->update('tracks', $data))
      {
          $response['code']=1;
          
      } 
      else
      {
        $response['code']=0;
      }
      return json_encode($response);
     
  }
	public function checkNewMovie($phone){
        $this->db->select('id');
        $this->db->or_where('movie_name',$phone);
        $this->db->from('movies');
        $count=$this->db->count_all_results();
        return $count;   
    }
    
      
        
   
    public function get_movieinformation()
    {
      $this->db->select('*');
      $this->db->from('movies');  
      $returneddata=$this->db->get();
      $array=$returneddata->result_array();
      return $array;
    }
    public function get_userinformation($email)
    {
      $this->db->select('name,email,access_token');
      $this->db->or_where('email',$email);
      $this->db->from('users');  
      $returneddata=$this->db->get();
      $array=$returneddata->result_array();
      return $array;
    }
}
?>