<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8" />

  <title>Anil Labs - Codeigniter mail templates</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

</head>

<body>

<div>

  <div style="font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 32px;color: #41637e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header">
  <!-- 	<img style="border: 0;-ms-interpolation-mode: bicubic;display: block;Margin-left: auto;Margin-right: auto;max-width: 152px" src="http://www.anil2u.info/wp-content/uploads/2013/09/anil-kumar-panigrahi-blog.png" alt="" width="152" height="108"> -->
  </div>

<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">Hi Quoram Ventures ,</p>

<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;"> You Have a quote Request from <b><?php echo $userName; ?></b></p>

	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;"> Company Name:<b><?php echo $companyname; ?></b></p>
<span style="Margin-bottom: 25px"><span>
	<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;"> Details :</p>
<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 14px;line-height: 25px;"><b><?php echo $details; ?></b></p>
<hr/>
<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 14px;line-height: 25px;">
	QUORUM VENTURES LTD<br>
	Head office Nairobi : - NAS Office Suites Kilimani, Nairobi,<br>
	Tel: +254 202131761 | Email: info@quorumventures.co.ke | Website: www.quorumventures.co.ke
</p>
</div>

</body>

</html>
